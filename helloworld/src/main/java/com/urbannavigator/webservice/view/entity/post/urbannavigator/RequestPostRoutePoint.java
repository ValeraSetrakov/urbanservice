
package com.urbannavigator.webservice.view.entity.post.urbannavigator;


public class RequestPostRoutePoint {
    private int addressId;
    private String name;
    private int categoryId;
    private int typeId;
    private String summary;

    public int getAddressId() {
        return addressId;
    }

    public void setAddressId(int addressId) {
        this.addressId = addressId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    @Override
    public String toString() {
        return "RequestPostRoutePoint{" + "showPlaceId=" + addressId + ", name=" + name + ", categoryId=" + categoryId + ", typeId=" + typeId + ", summary=" + summary + '}';
    }

}
