/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.urbannavigator.webservice.view;

import com.urbannavigator.webservice.domain.route.IRouteControler;
import com.urbannavigator.webservice.data.entities.Route;
import com.urbannavigator.webservice.domain.entity.Response;
import com.urbannavigator.webservice.domain.route.RouteController;
import com.urbannavigator.webservice.data.datasources.sql.base.DataSourceCore;
import com.urbannavigator.webservice.data.datasources.sql.base.DataSourceException;
import com.urbannavigator.webservice.data.hibernate.HibernateUtil;
import com.urbannavigator.webservice.data.repository.RouteRepository;
import com.urbannavigator.webservice.view.entity.RequestRoute;
import com.urbannavigator.webservice.view.mapper.urvannavigator.route.RouteViewMapper;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author valera
 */
@Path("route")
public class RouteView {

    @Context
    private UriInfo context;

    private IRouteControler controler;
    private RouteViewMapper mapper;
    
    public RouteView() {
        controler = new RouteController(new RouteRepository(new DataSourceCore(HibernateUtil.getSessionFactory())));
        mapper = new RouteViewMapper();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response<List<RequestRoute>> getRoutesByCurrentPlace(
            @QueryParam("lan") double lan, 
            @QueryParam("lon") double lon) {
        try {
            return Response
                .Builder
                .createSuccessFrom(mapper.mapForGet(controler.getRoutesByCurrentPlace(lan, lon)));
        } catch (DataSourceException e) {
            return new Response<>(e.getLocalizedMessage());
        }
    }
    
    @GET
    @Path("country/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<List<RequestRoute>> getRoutesByCountry(@PathParam("id") long id) {
        try {
            return Response
                .Builder
                .createSuccessFrom(mapper.mapForGet(controler.getRoutesByCountry(id)));
        } catch (DataSourceException e) {
            return new Response<>(e.getLocalizedMessage());
        }
    }
    
    @GET
    @Path("town/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<List<RequestRoute>> getRoutesByTown(@PathParam("id") long id) {
        try {
            return Response
                .Builder
                .createSuccessFrom(mapper.mapForGet(controler.getRoutesByTown(id)));
        } catch (DataSourceException e) {
            return new Response<>(e.getLocalizedMessage());
        }
    }
    
    @GET
    @Path("district/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<List<RequestRoute>> getRoutesByDistrict(@PathParam("id") long id) {
        try {
            return Response
                .Builder
                .createSuccessFrom(mapper.mapForGet(controler.getRoutesByDistrict(id)));
        } catch (DataSourceException e) {
            return new Response<>(e.getLocalizedMessage());
        }
    }
    
    @GET
    @Path("street/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<List<RequestRoute>> getRoutesByStreet(@PathParam("id") long id) {
        try {
            return Response
                .Builder
                .createSuccessFrom(mapper.mapForGet(controler.getRoutesByStreet(id)));
        } catch (DataSourceException e) {
            return new Response<>(e.getLocalizedMessage());
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response postRoute(RequestRoute requestRoute) {
        try {
            Route route = mapper.mapForPost(requestRoute);
            return controler.saveObject(route);
        } catch (DataSourceException e) {
            return new Response(e.getLocalizedMessage());
        }
    }
}
