
package com.urbannavigator.webservice.view.entity;

import java.util.Collection;


public class RequestRoutePoint extends RequestContentOwner{
    private Collection<RequestContent> contents;
    private double lat;
    private double lon;

    public RequestRoutePoint() {
    }
    
    public Collection<RequestContent> getContents() {
        return contents;
    }

    public void setContents(Collection<RequestContent> contents) {
        this.contents = contents;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    @Override
    public String toString() {
        return "RequestGetRoutePoint{" + "contents=" + contents + ", lan=" + lat + ", lon=" + lon + '}';
    }
}
