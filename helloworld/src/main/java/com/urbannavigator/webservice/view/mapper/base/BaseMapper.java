
package com.urbannavigator.webservice.view.mapper.base;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;


public abstract class BaseMapper<FROM, TO> 
        implements IMapperList<FROM, TO>, IReverseMapperList<FROM, TO>{
    protected abstract TO createTo();
    protected abstract FROM createFrom();
    
    @Override
    public TO map (FROM from) {
        return createTo();
    }
    @Override
    public List<TO> map(Collection<FROM> froms) {
        return froms.stream().map(this::map).collect(Collectors.toList());
    }
    
    @Override
    public FROM reverseMap(TO to) {
        return createFrom();
    }

    @Override
    public List<FROM> reverseMap(Collection<TO> tos) {
        return tos.stream().map(this::reverseMap).collect(Collectors.toList());
    }

}
