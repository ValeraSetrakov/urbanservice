
package com.urbannavigator.webservice.view.mapper.urvannavigator.get;

import com.urbannavigator.webservice.data.entities.RoutePoint;
import com.urbannavigator.webservice.view.entity.RequestRoutePoint;

public class FromRoutePointToRequestGetRoutePointMapper extends FromContentOwnerToRequestContentOwner<RoutePoint, RequestRoutePoint>{

    public FromRoutePointToRequestGetRoutePointMapper(FromContentToRequestGetContentMapper mapper) {
        super(mapper);
    }

    @Override
    protected RequestRoutePoint createTo() {
        return new RequestRoutePoint();
    }
    
    @Override
    public RequestRoutePoint map(RoutePoint from) {
        RequestRoutePoint to = super.map(from);
        to.setContents(fromContentToRequestGetContentMapper.map(from.getContents()));
        return to;
    }

    @Override
    protected RoutePoint createFrom() {
        //TODO need end!
        return null;
    }
    
    
}
