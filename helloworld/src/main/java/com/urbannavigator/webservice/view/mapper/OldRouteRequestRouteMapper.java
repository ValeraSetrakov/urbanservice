
package com.urbannavigator.webservice.view.mapper;

import com.urbannavigator.webservice.data.entities.Category;
import com.urbannavigator.webservice.data.entities.Route;
import com.urbannavigator.webservice.data.entities.Type;
import com.urbannavigator.webservice.view.entity.RequestCategory;
import com.urbannavigator.webservice.view.entity.RequestRoute;
import com.urbannavigator.webservice.view.entity.RequestType;
import com.urbannavigator.webservice.view.mapper.base.BaseMapper;

@Deprecated
public class OldRouteRequestRouteMapper extends BaseMapper<Route, RequestRoute>{
    
    private OldRouteRequestRoutePointMapper oldRouteRequestRoutePointMapper;

    public OldRouteRequestRouteMapper(OldRouteRequestRoutePointMapper oldRouteRequestRoutePointMapper) {
        this.oldRouteRequestRoutePointMapper = oldRouteRequestRoutePointMapper;
    }

    @Override
    protected RequestRoute createTo() {
        return new RequestRoute();
    }

    @Override
    protected Route createFrom() {
        return new Route();
    }

    @Override
    public RequestRoute map(Route from) {
        RequestRoute requestRoute = super.map(from); //To change body of generated methods, choose Tools | Templates.
        requestRoute.setId(from.getId());
        requestRoute.setName(from.getName());
        RequestCategory category = new RequestCategory();
        if(category != null) {
            category.setId(from.getType().getId());
            category.setName(from.getType().getName());
            requestRoute.setCategory(category);
        }
        RequestType type = new RequestType();
        if (type != null) {
            type.setId(from.getType().getId());
            type.setName(from.getType().getName());
            requestRoute.setType(type);
        }
        requestRoute.setPoints(oldRouteRequestRoutePointMapper.map(from.getRoutePoints()));
        return requestRoute;
    }

    @Override
    public Route reverseMap(RequestRoute to) {
        Route route = super.reverseMap(to); //To change body of generated methods, choose Tools | Templates.
        route.setId(to.getId());
        route.setName(to.getName());
        if(to.getCategory() != null) {
            Category category = new Category();
            category.setId(to.getType().getId());
            category.setName(to.getType().getName());
            route.setCategory(category);
        }
        if(to.getType() != null) {
            Type type = new Type();
            type.setId(to.getType().getId());
            type.setName(to.getType().getName());
            route.setType(type);
        }
        
        
        route.setRoutePoints(oldRouteRequestRoutePointMapper.reverseMap(to.getPoints()));
        return route;
    }
    
}
