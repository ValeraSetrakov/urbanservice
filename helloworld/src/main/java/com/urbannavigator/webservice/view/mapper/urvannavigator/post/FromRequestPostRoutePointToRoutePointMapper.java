
package com.urbannavigator.webservice.view.mapper.urvannavigator.post;

import com.urbannavigator.webservice.data.entities.Address;
import com.urbannavigator.webservice.data.entities.Content;
import com.urbannavigator.webservice.data.entities.Description;
import com.urbannavigator.webservice.data.entities.RoutePoint;
import com.urbannavigator.webservice.view.entity.post.urbannavigator.RequestPostRoutePoint;
import com.urbannavigator.webservice.view.mapper.base.BaseMapper;
import java.util.ArrayList;
import java.util.List;


public class FromRequestPostRoutePointToRoutePointMapper extends BaseMapper<RequestPostRoutePoint, RoutePoint>{

    @Override
    public RoutePoint map(RequestPostRoutePoint from) {
        RoutePoint routePoint = createTo();
        routePoint.setName(from.getName());
        String summary = from.getSummary();
        Description description = new Description();
        description.setContentOwner(routePoint);
        List<Content> contents = new ArrayList<>();
        description.setSummary(summary);
        contents.add(description);
        routePoint.setContents(contents);
        Address address = new Address();
        address.setId(from.getAddressId());
        routePoint.setAddress(address);
        
        return routePoint;
    }

    @Override
    protected RoutePoint createTo() {
        return new RoutePoint();
    }

    @Override
    protected RequestPostRoutePoint createFrom() {
        //TODO need end!
        return null;
    }
    
    
    
}
