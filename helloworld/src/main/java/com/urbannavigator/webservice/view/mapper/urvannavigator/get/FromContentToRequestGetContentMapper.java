
package com.urbannavigator.webservice.view.mapper.urvannavigator.get;

import com.urbannavigator.webservice.data.entities.Content;
import com.urbannavigator.webservice.view.entity.RequestContent;

public class FromContentToRequestGetContentMapper 
        extends FromDataBaseObjectToRequestIdObject<Content, RequestContent>{

    @Override
    protected RequestContent createTo() {
        return new RequestContent();
    }

    @Override
    public RequestContent map(Content from) {
        RequestContent to = super.map(from);
        long id = from.getId();
        to.setId(id);
        to.setValue(from.getValue());
        return to;
    }

    @Override
    public Content reverseMap(RequestContent to) {
        Content content = super.reverseMap(to); //To change body of generated methods, choose Tools | Templates.
        content.setValue(to.getValue());
        return content;
    }

    @Override
    protected Content createFrom() {
        //TODO end this part
        return null;
    }
    
    

}
