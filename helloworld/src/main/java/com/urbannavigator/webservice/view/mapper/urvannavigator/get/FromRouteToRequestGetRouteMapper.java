
package com.urbannavigator.webservice.view.mapper.urvannavigator.get;

import com.urbannavigator.webservice.data.entities.Route;
import com.urbannavigator.webservice.view.entity.RequestRoute;

public final class FromRouteToRequestGetRouteMapper extends FromTypeObjectToRequestTypeObject<Route, RequestRoute>{
    private final FromRoutePointToRequestGetRoutePointMapper fromRoutePointToRequestGetRoutePointMapper;

    public FromRouteToRequestGetRouteMapper(FromRoutePointToRequestGetRoutePointMapper fromRoutePointToRequestGetRoutePointMapper, TypeRequestTypeMapper typeRequestTypeMapper, CategoryRequestCategoryMapper categoryRequestCategoryMapper, FromContentToRequestGetContentMapper mapper) {
        super(typeRequestTypeMapper, categoryRequestCategoryMapper, mapper);
        this.fromRoutePointToRequestGetRoutePointMapper = fromRoutePointToRequestGetRoutePointMapper;
    }
    
    @Override
    protected RequestRoute createTo() {
        return new RequestRoute();
    }

    @Override
    public RequestRoute map(Route from) {
        RequestRoute to = super.map(from);
        to.setPoints(fromRoutePointToRequestGetRoutePointMapper.map(from.getRoutePoints()));
        return to;
    }

    @Override
    public Route reverseMap(RequestRoute to) {
        Route route = super.reverseMap(to); //To change body of generated methods, choose Tools | Templates.
        route.setRoutePoints(fromRoutePointToRequestGetRoutePointMapper.reverseMap(to.getPoints()));
        return route;
    }

    @Override
    protected Route createFrom() {
        return new Route();
    }
}
