
package com.urbannavigator.webservice.view.entity;


public abstract class RequestNameObject extends RequestIdObject{
    private String name;

    public RequestNameObject() {
    }

    public RequestNameObject(String name) {
        this.name = name;
    }

    public RequestNameObject(String name, long id) {
        super(id);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "RequestNameObject{" + '}';
    }
}
