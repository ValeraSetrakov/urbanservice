
package com.urbannavigator.webservice.view.mapper;

import com.urbannavigator.webservice.data.entities.RoutePoint;
import com.urbannavigator.webservice.view.entity.RequestRoutePoint;
import com.urbannavigator.webservice.view.mapper.base.BaseMapper;

@Deprecated
public class OldRouteRequestRoutePointMapper extends BaseMapper<RoutePoint, RequestRoutePoint>{

    @Override
    protected RequestRoutePoint createTo() {
        return new RequestRoutePoint();
    }

    @Override
    protected RoutePoint createFrom() {
        return new RoutePoint();
    }

    @Override
    public RequestRoutePoint map(RoutePoint from) {
        RequestRoutePoint requestRoutePoint = super.map(from); //To change body of generated methods, choose Tools | Templates.
        requestRoutePoint.setId(from.getId());
        requestRoutePoint.setLat(from.getAddress().getShowPlace().getCoordinate().getLatitude());
        requestRoutePoint.setLon(from.getAddress().getShowPlace().getCoordinate().getLatitude());
        requestRoutePoint.setName(from.getName());
        //TODO map contents
        return requestRoutePoint;
    }

    @Override
    public RoutePoint reverseMap(RequestRoutePoint to) {
        RoutePoint routePoint = super.reverseMap(to); //To change body of generated methods, choose Tools | Templates.
        routePoint.setId(to.getId());
        routePoint.setName(to.getName());
        //TODO map contents
        return routePoint;
    }
    
    
    

}
