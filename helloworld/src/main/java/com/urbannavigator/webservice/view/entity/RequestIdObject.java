
package com.urbannavigator.webservice.view.entity;


public class RequestIdObject {
    private long id;

    public RequestIdObject() {
    }

    public RequestIdObject(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "RequestIdObject{" + "id=" + id + '}';
    }
}
