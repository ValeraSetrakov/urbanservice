
package com.urbannavigator.webservice.view.mapper.urvannavigator.get;

import com.urbannavigator.webservice.data.entities.NameDataBaseObject;
import com.urbannavigator.webservice.view.entity.RequestNameObject;

public abstract class FromNameDataBaseObjectToRequestNameObject<FROM extends NameDataBaseObject, TO extends RequestNameObject> extends FromDataBaseObjectToRequestIdObject<FROM, TO>{

    @Override
    public TO map(FROM from) {
        TO to = super.map(from); //To change body of generated methods, choose Tools | Templates.
        to.setName(from.getName());
        return to;
    }

    @Override
    public FROM reverseMap(TO to) {
        FROM from = super.reverseMap(to); //To change body of generated methods, choose Tools | Templates.
        from.setName(to.getName());
        return from;
    }
    
    
    
}
