
package com.urbannavigator.webservice.view.entity.post.urbannavigator;

import java.util.List;

public class RequestPostRoute {
    private List<RequestPostRoutePoint> requestPostRoutePoints;
    private String name;
    private int categoryId;
    private int typeId;
    private String summary;

    public List<RequestPostRoutePoint> getRequestPostRoutePoints() {
        return requestPostRoutePoints;
    }

    public void setRequestPostRoutePoints(List<RequestPostRoutePoint> requestPostRoutePoints) {
        this.requestPostRoutePoints = requestPostRoutePoints;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }
    
    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    @Override
    public String toString() {
        return "RequestPostRoute{" + "requestPostRoutePoints=" + requestPostRoutePoints + ", name=" + name + ", categoryId=" + categoryId + ", typeId=" + typeId + ", summary=" + summary + '}';
    }

}
