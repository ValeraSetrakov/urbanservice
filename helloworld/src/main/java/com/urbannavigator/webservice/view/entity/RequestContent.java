
package com.urbannavigator.webservice.view.entity;

import com.urbannavigator.webservice.view.entity.RequestIdObject;



public class RequestContent extends RequestIdObject{
    private String value;

    public RequestContent() {
    }

    public RequestContent(long id, String value) {
        super(id);
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "RequestGetContent{" + "value=" + value + '}';
    }
}
