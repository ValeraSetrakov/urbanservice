
package com.urbannavigator.webservice.view.entity.post.urbannavigator;

import com.urbannavigator.webservice.data.entities.Bound;
import com.urbannavigator.webservice.data.entities.Coordinate;


public class RequestPostShowPlace {
    private int streetId;
    private String name;
    private String summary;
    private Bound bound;
    private Coordinate coordinate;

    public int getStreetId() {
        return streetId;
    }

    public void setStreetId(int streetId) {
        this.streetId = streetId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public Bound getBound() {
        return bound;
    }

    public void setBound(Bound bound) {
        this.bound = bound;
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(Coordinate coordinate) {
        this.coordinate = coordinate;
    }

    @Override
    public String toString() {
        return "RequestPostShowPlace{" + "streetId=" + streetId + ", name=" + name + ", summary=" + summary + ", bound=" + bound + ", coordinate=" + coordinate + '}';
    }
}
