
package com.urbannavigator.webservice.view.mapper.urvannavigator.get;

import com.urbannavigator.webservice.data.entities.Type;
import com.urbannavigator.webservice.data.entities.TypeObject;
import com.urbannavigator.webservice.view.entity.RequestNameObject;
import com.urbannavigator.webservice.view.entity.RequestType;


public class TypeRequestTypeMapper extends FromNameDataBaseObjectToRequestNameObject<Type, RequestType>{

    @Override
    protected RequestType createTo() {
        return new RequestType();
    }

    @Override
    protected Type createFrom() {
        return new Type();
    }
    
}
