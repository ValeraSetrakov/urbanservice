
package com.urbannavigator.webservice.view.entity;


public class RequestTypeObject extends RequestCategoryObject{
    private RequestType type;

    public RequestTypeObject() {
    }

    public RequestType getType() {
        return type;
    }

    public void setType(RequestType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "RequestTypeObject{" + "type=" + type + '}';
    }
}
