
package com.urbannavigator.webservice.view.mapper.urvannavigator.get;

import com.urbannavigator.webservice.data.entities.ObjectWithContent;
import com.urbannavigator.webservice.view.entity.RequestContentOwner;

public abstract class FromContentOwnerToRequestContentOwner<FROM extends ObjectWithContent, TO extends RequestContentOwner> extends FromNameDataBaseObjectToRequestNameObject<FROM, TO>{

    protected final FromContentToRequestGetContentMapper fromContentToRequestGetContentMapper;

    public FromContentOwnerToRequestContentOwner(FromContentToRequestGetContentMapper mapper) {
        this.fromContentToRequestGetContentMapper = mapper;
    }

    @Override
    public TO map(FROM from) {
        TO to = super.map(from); //To change body of generated methods, choose Tools | Templates.
        to.setContents(fromContentToRequestGetContentMapper.map(from.getContents()));
        return to;
    }

    @Override
    public FROM reverseMap(TO to) {
        FROM from = super.reverseMap(to); //To change body of generated methods, choose Tools | Templates.
        from.setContents(fromContentToRequestGetContentMapper.reverseMap(to.getContents()));
        return from;
    }
    
    
}
