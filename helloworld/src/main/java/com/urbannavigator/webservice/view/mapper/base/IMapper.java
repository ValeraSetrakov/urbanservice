
package com.urbannavigator.webservice.view.mapper.base;


public interface IMapper<FROM, TO> {
    TO map(FROM from);
}
