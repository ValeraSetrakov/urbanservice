
package com.urbannavigator.webservice.view.mapper.base;

import java.util.Collection;
import java.util.List;


public interface IReverseMapperList<FROM, TO> extends IReverseMapper<FROM, TO>{
    List<FROM> reverseMap(Collection<TO> from);
}
