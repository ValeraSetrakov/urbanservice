
package com.urbannavigator.webservice.view.mapper.base;


public interface IReverseMapper<FROM, TO> {
    FROM reverseMap(TO from);
}
