
package com.urbannavigator.webservice.view.mapper.urvannavigator.post;

import com.urbannavigator.webservice.data.entities.Content;
import com.urbannavigator.webservice.data.entities.Description;
import com.urbannavigator.webservice.data.entities.ShowPlace;
import com.urbannavigator.webservice.data.entities.Street;
import com.urbannavigator.webservice.view.entity.post.urbannavigator.RequestPostShowPlace;
import com.urbannavigator.webservice.view.mapper.base.BaseMapper;
import java.util.ArrayList;
import java.util.List;


public class FromRequestPostShowPlaceToShowPlaceMapper extends BaseMapper<RequestPostShowPlace, ShowPlace>{

    @Override
    protected ShowPlace createTo() {
        return new ShowPlace();
    }
    
    @Override
    public ShowPlace map(RequestPostShowPlace from) {
        ShowPlace showPlace = createTo();
        showPlace.setBound(from.getBound());
        showPlace.setCoordinate(from.getCoordinate());
        showPlace.setName(from.getName());
        Street street = new Street();
        street.setId(from.getStreetId());
        showPlace.setStreet(street);
        Description description = new Description();
        description.setSummary(from.getSummary());
        List<Content> contents = new ArrayList<>();
        contents.add(description);
        showPlace.setContents(contents);
        return showPlace;
    }

    @Override
    protected RequestPostShowPlace createFrom() {
        //TODO need end!
        return null;
    }
}
