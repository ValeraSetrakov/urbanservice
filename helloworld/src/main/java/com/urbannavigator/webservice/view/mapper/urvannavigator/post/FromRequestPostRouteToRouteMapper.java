
package com.urbannavigator.webservice.view.mapper.urvannavigator.post;

import com.urbannavigator.webservice.data.entities.Category;
import com.urbannavigator.webservice.data.entities.Content;
import com.urbannavigator.webservice.data.entities.Description;
import com.urbannavigator.webservice.data.entities.Route;
import com.urbannavigator.webservice.data.entities.RoutePoint;
import com.urbannavigator.webservice.data.entities.Type;
import com.urbannavigator.webservice.view.entity.post.urbannavigator.RequestPostRoute;
import com.urbannavigator.webservice.view.mapper.base.BaseMapper;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;


public class FromRequestPostRouteToRouteMapper extends BaseMapper<RequestPostRoute, Route>{
    
    private FromRequestPostRoutePointToRoutePointMapper routePointMapper;

    public FromRequestPostRouteToRouteMapper(FromRequestPostRoutePointToRoutePointMapper routePointMapper) {
        this.routePointMapper = routePointMapper;
    }

    @Override
    public Route map(RequestPostRoute from) {
        Route route = createTo();
        String summary = from.getSummary();
        Description description = new Description();
        List<Content> contents = new ArrayList<>();
        contents.add(description);
        description.setSummary(summary);
        description.setContentOwner(route);
        route.setName(from.getName());
        Collection<RoutePoint> routePoints = routePointMapper.map(from.getRequestPostRoutePoints());
        route.setRoutePoints(new ArrayList<>(routePoints));
        route.setContents(contents);
        Category category = new Category();
        category.setId(from.getCategoryId());
        Type type = new Type();
        type.setId(from.getTypeId());
        route.setCategory(category);
        route.setType(type);
        return route;
    }

    @Override
    protected Route createTo() {
        return new Route();
    }

    @Override
    protected RequestPostRoute createFrom() {
        //TODO need end!
        return null;
    }
    
    
    
}
