
package com.urbannavigator.webservice.view.mapper.urvannavigator.route;

import com.urbannavigator.webservice.data.entities.Route;
import com.urbannavigator.webservice.view.entity.RequestRoute;
import com.urbannavigator.webservice.view.mapper.OldRouteRequestRouteMapper;
import com.urbannavigator.webservice.view.mapper.OldRouteRequestRoutePointMapper;
import com.urbannavigator.webservice.view.mapper.base.BaseMapper;
import java.util.Collection;
import java.util.List;


public class RouteViewMapper {
    BaseMapper<Route, RequestRoute> mapper;

    public RouteViewMapper() {
        this.mapper = new OldRouteRequestRouteMapper(new OldRouteRequestRoutePointMapper());
    }

    public RouteViewMapper(BaseMapper<Route, RequestRoute> mapper) {
        this.mapper = mapper;
    }
    
    public Route mapForPost (RequestRoute requestRoute) {
        return mapper.reverseMap(requestRoute);
    }
    
    public RequestRoute mapForGet (Route route) {
        return mapper.map(route);
    }
    
    public Collection<Route> mapForPost (Collection<RequestRoute> requestRoutes) {
        return mapper.reverseMap(requestRoutes);
    }
    
    public List<RequestRoute> mapForGet (Collection<Route> route) {
        return mapper.map(route);
    }
}
