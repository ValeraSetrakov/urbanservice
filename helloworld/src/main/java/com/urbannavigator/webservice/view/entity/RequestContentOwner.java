
package com.urbannavigator.webservice.view.entity;

import java.util.Collection;


public class RequestContentOwner extends RequestNameObject{
    private Collection<RequestContent> contents;

    public Collection<RequestContent> getContents() {
        return contents;
    }

    public void setContents(Collection<RequestContent> contents) {
        this.contents = contents;
    }

    @Override
    public String toString() {
        return "RequestContentOwner{" + "contents=" + contents + '}';
    }
}
