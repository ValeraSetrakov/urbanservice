
package com.urbannavigator.webservice.view.entity;


public class RequestCategoryObject extends RequestContentOwner{
    private RequestCategory category;

    public RequestCategoryObject() {
    }

    public RequestCategory getCategory() {
        return category;
    }

    public void setCategory(RequestCategory category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "RequestCategoryObject{" + "category=" + category + '}';
    }
}
