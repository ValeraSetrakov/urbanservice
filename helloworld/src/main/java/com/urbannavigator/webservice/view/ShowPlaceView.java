/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.urbannavigator.webservice.view;

import com.urbannavigator.webservice.domain.showplace.IShowPlaceController;
import com.urbannavigator.webservice.data.entities.ShowPlace;
import com.urbannavigator.webservice.domain.entity.Response;
import com.urbannavigator.webservice.domain.showplace.ShowPlaceController;
import com.urbannavigator.webservice.data.datasources.sql.base.DataSourceCore;
import com.urbannavigator.webservice.data.datasources.sql.base.DataSourceException;
import com.urbannavigator.webservice.data.hibernate.HibernateUtil;
import com.urbannavigator.webservice.data.repository.ShowPlaceRepository;
import com.urbannavigator.webservice.view.entity.RequestShowPlace;
import com.urbannavigator.webservice.view.mapper.OldShowPlaceRequestShowPlaceMapper;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author valera
 */
@Path("showplace")
public class ShowPlaceView {

    @Context
    private UriInfo context;
    
    private final IShowPlaceController controller;
    private OldShowPlaceRequestShowPlaceMapper mapper;

    public ShowPlaceView() {
        controller = new ShowPlaceController(new ShowPlaceRepository(new DataSourceCore(HibernateUtil.getSessionFactory())));
        mapper = new OldShowPlaceRequestShowPlaceMapper();
    }

   
    @GET
    @Path("showplaces")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<List<RequestShowPlace>> getShowPlacesByCurrentPlace(
            @QueryParam("lan") double lan, 
            @QueryParam("lon") double lon) {
        try {
            return new Response<>(mapper.map(controller.getShowPlacesByCurrentPlace(lan, lon)));
        } catch (DataSourceException e) {
            return new Response<>(e.getLocalizedMessage());
        }
    }
    
    @GET
    @Path("country/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<List<RequestShowPlace>> getShowPlacesByCountry(@PathParam("id") long id) {
        try {
            return new Response<>(mapper.map(controller.getShowPlacesByCountry(id)));
        } catch (DataSourceException e) {
            return new Response<>(e.getLocalizedMessage());
        }
    }
    
    @GET
    @Path("town/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<List<RequestShowPlace>> getShowPlacesByTown(@PathParam("id") long id) {
        try {
            return new Response<>(mapper.map(controller.getShowPlacesByTown(id)));
        } catch (DataSourceException e) {
            return new Response<>(e.getLocalizedMessage());
        }
    }
    
    @GET
    @Path("district/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<List<RequestShowPlace>> getShowPlacesByDistrict(@PathParam("id") long id) {
        try {
            return new Response<>(mapper.map(controller.getShowPlacesByDistrict(id)));
        } catch (DataSourceException e) {
            return new Response<>(e.getLocalizedMessage());
        }
    }
    
    @GET
    @Path("street/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<List<RequestShowPlace>> getShowPlacesByStreet(@PathParam("id") long id) {
        try {
            return new Response<>(mapper.map(controller.getShowPlacesByStreet(id)));
        } catch (DataSourceException e) {
            return new Response<>(e.getLocalizedMessage());
        }
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response postRoutesByStreet(RequestShowPlace requestShowPlace) {
        try {
            ShowPlace showPlace = mapper.reverseMap(requestShowPlace);
            System.out.println(showPlace);
            return controller.saveObject(showPlace);
        } catch ( DataSourceException e) {
            return new Response(e.getLocalizedMessage());
        }
    }
}
