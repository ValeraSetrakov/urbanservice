
package com.urbannavigator.webservice.view.entity;


public class RequestShowPlace {
    private long id;
    private String name;
    private long streetId;
    private long districtId;
    private long townId;
    private long countryId;
    private double lat;
    private double lon;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getStreetId() {
        return streetId;
    }

    public void setStreetId(long streetId) {
        this.streetId = streetId;
    }

    public long getDistrictId() {
        return districtId;
    }

    public void setDistrictId(long districtId) {
        this.districtId = districtId;
    }

    public long getTownId() {
        return townId;
    }

    public void setTownId(long townId) {
        this.townId = townId;
    }

    public long getCountryId() {
        return countryId;
    }

    public void setCountryId(long countryId) {
        this.countryId = countryId;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    @Override
    public String toString() {
        return "RequestShowPlace{" + "id=" + id + ", name=" + name + ", streetId=" + streetId + ", districtId=" + districtId + ", townId=" + townId + ", countryId=" + countryId + ", lat=" + lat + ", lon=" + lon + '}';
    }

}
