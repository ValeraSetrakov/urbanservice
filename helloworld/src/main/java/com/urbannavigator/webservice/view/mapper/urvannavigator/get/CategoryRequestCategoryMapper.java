
package com.urbannavigator.webservice.view.mapper.urvannavigator.get;

import com.urbannavigator.webservice.data.entities.Category;
import com.urbannavigator.webservice.view.entity.RequestCategory;

public class CategoryRequestCategoryMapper extends FromNameDataBaseObjectToRequestNameObject<Category, RequestCategory>{

    @Override
    protected RequestCategory createTo() {
        return new RequestCategory();
    }

    @Override
    protected Category createFrom() {
        return new Category();
    }

    

}
