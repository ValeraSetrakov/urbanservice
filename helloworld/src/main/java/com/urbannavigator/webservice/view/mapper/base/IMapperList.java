
package com.urbannavigator.webservice.view.mapper.base;

import java.util.Collection;
import java.util.List;


public interface IMapperList<FROM, TO> extends IMapper<FROM, TO>{
    List<TO> map(Collection<FROM> from);
}
