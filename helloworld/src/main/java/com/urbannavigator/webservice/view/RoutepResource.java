/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.urbannavigator.webservice.view;

import com.urbannavigator.webservice.domain.entity.Response;
import com.urbannavigator.webservice.domain.routepoint.IRoutePointController;
import com.urbannavigator.webservice.domain.routepoint.RoutePointController;
import com.urbannavigator.webservice.data.datasources.sql.base.DataSourceCore;
import com.urbannavigator.webservice.data.hibernate.HibernateUtil;
import com.urbannavigator.webservice.data.repository.RoutePointRepository;
import com.urbannavigator.webservice.view.entity.post.urbannavigator.RequestPostRoutePoint;
import com.urbannavigator.webservice.view.mapper.urvannavigator.post.FromRequestPostRoutePointToRoutePointMapper;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author valera
 */
@Path("routepoint")
public class RoutepResource {
    
    

    @Context
    private UriInfo context;
    
    IRoutePointController controller;
    FromRequestPostRoutePointToRoutePointMapper mapper = new FromRequestPostRoutePointToRoutePointMapper();

    /**
     * Creates a new instance of RoutepointResource
     */
    public RoutepResource() {
        controller = new RoutePointController(new RoutePointRepository(new DataSourceCore(HibernateUtil.getSessionFactory())));
    }

    /**
     * Retrieves representation of an instance of com.urbannavigator.webservice.base.RoutepResource
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson() {
        //TODO return proper representation object
        throw new UnsupportedOperationException();
    }

    /**
     * PUT method for updating or creating an instance of RoutepResource
     * @param content representation for the resource
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response putJson(RequestPostRoutePoint requestPostRoutePoint) {
        return controller.saveRoutePoint(mapper.map(requestPostRoutePoint));
    }
}
