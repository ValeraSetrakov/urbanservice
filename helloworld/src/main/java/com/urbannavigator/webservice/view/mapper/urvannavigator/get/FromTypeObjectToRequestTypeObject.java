
package com.urbannavigator.webservice.view.mapper.urvannavigator.get;

import com.urbannavigator.webservice.data.entities.TypeObject;
import com.urbannavigator.webservice.view.entity.RequestTypeObject;


public abstract class FromTypeObjectToRequestTypeObject
        <FROM extends TypeObject, TO extends RequestTypeObject> 
        extends FromCategoryObjectToRequestCategoryObject<FROM, TO>{
    
    protected TypeRequestTypeMapper typeRequestTypeMapper;

    public FromTypeObjectToRequestTypeObject(TypeRequestTypeMapper typeRequestTypeMapper, CategoryRequestCategoryMapper categoryRequestCategoryMapper, FromContentToRequestGetContentMapper mapper) {
        super(categoryRequestCategoryMapper, mapper);
        this.typeRequestTypeMapper = typeRequestTypeMapper;
    }   

    @Override
    public TO map(FROM from) {
        TO to = super.map(from); //To change body of generated methods, choose Tools | Templates.
        to.setType(typeRequestTypeMapper.map(from.getType()));
        return to;
    }

    @Override
    public FROM reverseMap(TO to) {
        FROM from = super.reverseMap(to); //To change body of generated methods, choose Tools | Templates.
        from.setType(typeRequestTypeMapper.reverseMap(to.getType()));
        return from;
    }
    
    
    
}
