
package com.urbannavigator.webservice.view.mapper;

import com.urbannavigator.webservice.data.entities.ShowPlace;
import com.urbannavigator.webservice.view.entity.RequestShowPlace;
import com.urbannavigator.webservice.view.mapper.base.BaseMapper;

@Deprecated
public class OldShowPlaceRequestShowPlaceMapper extends BaseMapper<ShowPlace, RequestShowPlace>{

    @Override
    protected RequestShowPlace createTo() {
        return new RequestShowPlace();
    }

    @Override
    protected ShowPlace createFrom() {
        return new ShowPlace();
    }

    @Override
    public RequestShowPlace map(ShowPlace from) {
        RequestShowPlace requestShowPlace = super.map(from); //To change body of generated methods, choose Tools | Templates.
        requestShowPlace.setId(from.getId());
        requestShowPlace.setName(from.getName());
        requestShowPlace.setLat(from.getCoordinate().getLatitude());
        requestShowPlace.setLon(from.getCoordinate().getLongitude());
        requestShowPlace.setStreetId(from.getStreet().getId());
        requestShowPlace.setDistrictId(from.getStreet().getDistrict().getId());
        requestShowPlace.setTownId(from.getStreet().getDistrict().getTown().getId());
        requestShowPlace.setCountryId(from.getStreet().getDistrict().getTown().getCountry().getId());
        return requestShowPlace;
    }

    @Override
    public ShowPlace reverseMap(RequestShowPlace to) {
        return super.reverseMap(to); //To change body of generated methods, choose Tools | Templates.
        //TODO
    }
}
