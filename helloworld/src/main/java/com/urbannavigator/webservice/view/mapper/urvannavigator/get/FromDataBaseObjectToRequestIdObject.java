
package com.urbannavigator.webservice.view.mapper.urvannavigator.get;

import com.urbannavigator.webservice.data.entities.DataBaseObject;
import com.urbannavigator.webservice.view.entity.RequestIdObject;
import com.urbannavigator.webservice.view.mapper.base.BaseMapper;


public abstract class FromDataBaseObjectToRequestIdObject<FROM extends DataBaseObject, TO extends RequestIdObject> extends BaseMapper<FROM, TO>{

    @Override
    public TO map(FROM from) {
        TO to = super.map(from); //To change body of generated methods, choose Tools | Templates.
        to.setId(from.getId());
        return to;
    }

    @Override
    public FROM reverseMap(TO to) {
        FROM from = super.reverseMap(to); //To change body of generated methods, choose Tools | Templates.
        from.setId(to.getId());
        return from;
    }
    
}
