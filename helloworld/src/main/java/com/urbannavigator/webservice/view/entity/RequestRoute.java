
package com.urbannavigator.webservice.view.entity;

import com.urbannavigator.webservice.view.entity.RequestTypeObject;
import java.util.Collection;


public class RequestRoute extends RequestTypeObject{
    private Collection<RequestRoutePoint> points;
    private Collection<RequestContent> contents;

    public RequestRoute() {
    }
    
    public Collection<RequestRoutePoint> getPoints() {
        return points;
    }

    public void setPoints(Collection<RequestRoutePoint> points) {
        this.points = points;
    }

    public Collection<RequestContent> getContents() {
        return contents;
    }

    public void setContents(Collection<RequestContent> contents) {
        this.contents = contents;
    }

    @Override
    public String toString() {
        return "RequestGetRoute{" + "points=" + points + ", contents=" + contents + '}';
    }
}
