
package com.urbannavigator.webservice.view.mapper.urvannavigator.get;

import com.urbannavigator.webservice.data.entities.CategoryObject;
import com.urbannavigator.webservice.view.entity.RequestCategoryObject;

public abstract class FromCategoryObjectToRequestCategoryObject
        <FROM extends CategoryObject, TO extends RequestCategoryObject> 
        extends FromContentOwnerToRequestContentOwner<FROM, TO>{
    
    protected CategoryRequestCategoryMapper categoryRequestCategoryMapper;

    public FromCategoryObjectToRequestCategoryObject(CategoryRequestCategoryMapper categoryRequestCategoryMapper, FromContentToRequestGetContentMapper mapper) {
        super(mapper);
        this.categoryRequestCategoryMapper = categoryRequestCategoryMapper;
    }

    @Override
    public TO map(FROM from) {
        TO to = super.map(from); //To change body of generated methods, choose Tools | Templates.
        to.setCategory(categoryRequestCategoryMapper.map(from.getCategory()));
        return to;
    }

    @Override
    public FROM reverseMap(TO to) {
        FROM from = super.reverseMap(to); //To change body of generated methods, choose Tools | Templates.
        from.setCategory(categoryRequestCategoryMapper.reverseMap(to.getCategory()));
        return from;
    }

}
