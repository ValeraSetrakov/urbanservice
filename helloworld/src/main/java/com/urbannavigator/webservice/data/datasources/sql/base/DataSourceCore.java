
package com.urbannavigator.webservice.data.datasources.sql.base;

import com.urbannavigator.webservice.data.entities.DataBaseObject;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;


public class DataSourceCore implements IDataSource <DataBaseObject>{
    
    private SessionFactory sessionFactory;

    public DataSourceCore(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * insertAll item to database
     * @param item - object for insertAll 
     * @throws DataSourceException - throw this exception if session is null
     */
    @Override
    public void insert(DataBaseObject item) throws DataSourceException {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(item);
            session.getTransaction().commit();
        } catch (Throwable e) {
            throw new DataSourceException(e);
        }
    }

    /**
     * insertAll items to database
     * @param items - objects for insertAll 
     * @throws DataSourceException - throw this exception if session is null
     */
    @Override
    public void insert(DataBaseObject[] items) throws DataSourceException {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Arrays.stream(items).forEach(session::save);
            session.getTransaction().commit();
        } catch (Throwable e) {
            throw new DataSourceException(e);
        }
    }

    /**
     * insertAll items to database
     * @param items - objects for insertAll 
     * @throws DataSourceException - throw this exception if session is null
     */
    @Override
    public void insertAll(Collection<DataBaseObject> items) throws DataSourceException {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            items.forEach(session::save);
            session.getTransaction().commit();
        } catch (Throwable e) {
            throw new DataSourceException(e);
        }
    }
    
    /**
     * update item in database
     * @param item - object for update
     * @throws DataSourceException - throw this exception if session is null
     */
    @Override
    public void update(DataBaseObject item) throws DataSourceException{
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(item);
            session.getTransaction().commit();
        } catch (Throwable e) {
            throw new DataSourceException(e);
        }
        
    }

    /**
     * delete item from database
     * @param item - object for delete
     * @throws DataSourceException - throw this exception if session is null
     */
    @Override
    public void delete(DataBaseObject item) throws DataSourceException {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(item);
            session.getTransaction().commit();
        } catch (Throwable e) {
            throw new DataSourceException(e);
        }
    }

    /**
     * delete item from database
     * @param id - param for search object in database
     * @param c - type of item. Needed to define a table 
     * @throws DataSourceException - throw this exception if session is null
     */
    @Override
    public <T extends DataBaseObject> void deleteItemById(long id, Class<T> c) throws DataSourceException {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            T item = session.get(c, id);
            session.delete(item);
            session.getTransaction().commit();
        }
    }
    
    /**
     * select item from database
     * @param id - param for search object in database
     * @param c - type of item. Needed to define a table
     * @return T - result of search
     * @throws DataSourceException - throw this exception if session is null
     */
    @Override
    public <T extends DataBaseObject> T selectItem(long id, Class<T> c) 
            throws DataSourceException {
        try (Session session = sessionFactory.openSession()) {
            return session.get(c, id);
        } catch (Throwable e) {
            throw new DataSourceException(e);
        }
    }

    /**
     * select items from database
     * @param c - type of item. Needed to define a table
     * @return List<T> - result of search
     * @throws DataSourceException - throw this exception if session is null
     */
    @Override
    public <T extends DataBaseObject> List<T> selectAllItems(Class<T> c) throws DataSourceException {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("FROM " + c.getSimpleName(), c).list();
        } catch (Throwable e) {
            throw new DataSourceException(e);
        }
        
    }

    /**
     * select item from database
     * @param strQuery - query to database with params
     * @param params - param for search object in database
     * @return T - result of search
     * @throws DataSourceException - throw this exception if session is null
     */
    @Override
    public <T extends DataBaseObject> List<T> selectItem (
            String strQuery,
            Map<String, Object> params) throws DataSourceException {
        try (Session session = sessionFactory.openSession()) {
            Query<T> query = session.createQuery(strQuery);
            params.forEach(query::setParameter);
            return query.list();
        } catch (Throwable e) {
            throw new DataSourceException(e);
        }
        
    }

    @Override
    public void exec(Transaction transaction) throws DataSourceException {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            transaction.exec(session);
            session.getTransaction().commit();
        } catch (Throwable e) {
            throw new DataSourceException(e);
        }
        
    }
    
    
}
