package com.urbannavigator.webservice.data.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Content extends DataBaseObject{
    
    @ManyToOne
    private ObjectWithContent contentOwner;

    public Content() {
    }
    
    public ObjectWithContent getContentOwner() {
        return contentOwner;
    }

    public void setContentOwner(ObjectWithContent contentOwner) {
        this.contentOwner = contentOwner;
    }
    
    public abstract String getValue();
    public abstract void setValue(String value);

    @Override
    public String toString() {
        return "Content{" + "contentOwner=" + contentOwner + '}';
    }

   

}
