
package com.urbannavigator.webservice.data.datasources.sql.base;

public class DataSourceException extends Exception{

    public DataSourceException() {
    }

    public DataSourceException(String message) {
        super(message);
    }

    public DataSourceException(Throwable cause) {
        super(cause);
    }
    
}
