
package com.urbannavigator.webservice.data.entities;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class ObjectWithContent extends NameDataBaseObject {
    @OneToMany(mappedBy = "contentOwner", cascade = CascadeType.ALL)
    private List<Content> contents;

    public List<Content> getContents() {
        return contents;
    }

    public void setContents(List<Content> contents) {
        this.contents = contents;
    }

    @Override
    public String toString() {
        return "ObjectWithContent{" + "contents=" + contents + '}';
    }
}
