package com.urbannavigator.webservice.data.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Town extends Locality {

    @ManyToOne(fetch = FetchType.EAGER)
    @JsonIgnore
    private Country country;
    
    @OneToMany(mappedBy = "town", cascade = CascadeType.ALL)
    private List<District> districts = new ArrayList<>();
    
    public Town() {
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }
    
    public List<District> getDistricts() {
        return districts;
    }

    public void setDistricts(List<District> districts) {
        this.districts = districts;
    }

    @Override
    public String toString() {
        return "Town{" + "country=" + country + ", districts=" + districts + '}';
    }
    
}
