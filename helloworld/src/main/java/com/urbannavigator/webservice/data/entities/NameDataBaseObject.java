
package com.urbannavigator.webservice.data.entities;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class NameDataBaseObject extends DataBaseObject{
    
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "NameDataBaseObject{" + "name=" + name + '}';
    }

}
