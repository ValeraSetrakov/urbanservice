
package com.urbannavigator.webservice.data.repository;

import com.urbannavigator.webservice.base.TypeLocality;
import com.urbannavigator.webservice.domain.routepoint.IRoutePointRepository;
import com.urbannavigator.webservice.data.datasources.sql.base.DataSourceException;
import com.urbannavigator.webservice.data.datasources.sql.base.IDataSource;
import com.urbannavigator.webservice.data.entities.Address;
import com.urbannavigator.webservice.data.entities.Route;
import com.urbannavigator.webservice.data.entities.RoutePoint;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class RoutePointRepository extends CurrentLocalityRepository implements IRoutePointRepository{

    public RoutePointRepository(IDataSource iDataSource) {
        super(iDataSource);
    }

    @Override
    public RoutePoint getRoutePointById(long id) throws DataSourceException {
        return iDataSource.selectItem(id, RoutePoint.class);
    }

    @Override
    public List<RoutePoint> getRoutePointsByLocalityId(long id, TypeLocality typeLocality) throws DataSourceException {
        List<Address> addresses = getAddressByLocalityId(id, typeLocality);
        List<RoutePoint> routePoints = new ArrayList<>();
        addresses.forEach(address -> {
            try {
                routePoints.addAll(getRoutePointsByAddressId(address.getId()));
            } catch (DataSourceException e) {

            }
        });
        return routePoints;
    }
    
    

    @Override
    public List<RoutePoint> getRoutePointsByAddressId(long id) throws DataSourceException {
        String paramName = "address_id";
        String query = String.format("FROM routepoint WHERE %s=:%s", paramName, paramName);
        HashMap<String, Object> params = new HashMap<>(1);
        params.put(paramName, id);
        return iDataSource.selectItem(query, params);
    }

    @Override
    public void saveRoutePoint(RoutePoint routePoint) throws DataSourceException {
        iDataSource.insert(routePoint);
    }
    
    
    
    
}
