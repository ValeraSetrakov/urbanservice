
package com.urbannavigator.webservice.data.entities;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class GeoObject extends ObjectWithContent {
    
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "longitude", column = @Column(name = "center_longitude")),
            @AttributeOverride(name = "latitude", column = @Column(name = "center_latitude"))
    })
    private Coordinate coordinate;

    public GeoObject() {
    }

    public GeoObject(Coordinate coordinate) {
        this.coordinate = coordinate;
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(Coordinate coordinate) {
        this.coordinate = coordinate;
    }

    @Override
    public String toString() {
        return "GeoObject{" + "coordinate=" + coordinate + '}';
    }
}
