
package com.urbannavigator.webservice.data.repository;

import com.urbannavigator.webservice.base.TypeLocality;
import com.urbannavigator.webservice.data.datasources.sql.base.DataSourceException;
import com.urbannavigator.webservice.data.datasources.sql.base.IDataSource;
import com.urbannavigator.webservice.data.entities.Address;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class AddressRepository extends BaseRepository{

    public AddressRepository(IDataSource iDataSource) {
        super(iDataSource);
    }
    
    public List<Address> getAddressByLocalityId (long id, TypeLocality typeLocality) throws DataSourceException{
        String strFormTypeLocality = "";
        switch (typeLocality) {
            case TypeCountry: {
                strFormTypeLocality = "country_id";
                break;
            }
            case TypeTown: {
                strFormTypeLocality = "town_id";
                break;
            }
            case TypeDistrict: {
                strFormTypeLocality = "district_id";
                break;
            }
            case TypeStreet: {
                strFormTypeLocality = "street_id";
                break;
            }
            case TypeShowPlace: {
                strFormTypeLocality = "showPlace_id";
                break;
            }
            default: throw new DataSourceException("Not found such type of locality");
        }
        String query = String.format("FROM Address WHERE %s = :%s", strFormTypeLocality, strFormTypeLocality);
        Map<String, Object> params = new HashMap<>(1);
        params.put(strFormTypeLocality, id);
        List<Address> addresses = iDataSource.selectItem(query, params);
        checkReturnList(addresses, Address.class);
        return addresses;
    }
}
