
package com.urbannavigator.webservice.data.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@Entity
public class ShowPlace extends Locality {
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JsonIgnore
    private Street street;

    public Street getStreet() {
        return street;
    }

    public void setStreet(Street street) {
        this.street = street;
    }

    @Override
    public String toString() {
        return "StreetObject{" + "street=" + street + '}';
    }
}
