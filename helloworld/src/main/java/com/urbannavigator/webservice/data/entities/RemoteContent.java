
package com.urbannavigator.webservice.data.entities;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class RemoteContent extends Content{
    
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String getValue() {
        return  getUrl();
    }

    @Override
    public void setValue(String value) {
        this.url = value;
    }

    @Override
    public String toString() {
        return "RemoteContent{" + "url=" + url + '}';
    }
}
