
package com.urbannavigator.webservice.data.entities;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;


public class Bound {
    
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "longitude", column = @Column(name = "leftTop_longitude")),
            @AttributeOverride(name = "latitude", column = @Column(name = "leftTop_latitude"))
    })
    private Coordinate northeast;
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "longitude", column = @Column(name = "rightBottom_longitude")),
            @AttributeOverride(name = "latitude", column = @Column(name = "rightBottom_latitude"))
    })
    private Coordinate southwest;

    public Bound(Coordinate rightBottomCoordinate, Coordinate leftTopCoordinate) {
        this.northeast = leftTopCoordinate;
        this.southwest = rightBottomCoordinate;
    }

    public Bound() {
    }

    public Coordinate getNortheast() {
        return northeast;
    }

    public void setNortheast(Coordinate northeast) {
        this.northeast = northeast;
    }

    public Coordinate getSouthwest() {
        return southwest;
    }

    public void setSouthwest(Coordinate southwest) {
        this.southwest = southwest;
    }

    @Override
    public String toString() {
        return "Bound{" + "leftTopCoordinate=" + northeast + ", rightBottomCoordinate=" + southwest + '}';
    }
    
    
}
