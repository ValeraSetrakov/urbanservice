
package com.urbannavigator.webservice.data.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class Address extends DataBaseObject {
    
    @ManyToOne
    @JsonIgnore
    private Country country;
    
    @ManyToOne
    @JsonIgnore
    private Town town;
    
    @ManyToOne
    @JsonIgnore
    private District district;
    
    @ManyToOne
    @JsonIgnore
    private Street street;
    
    @OneToOne
    @JsonIgnore
    private ShowPlace showPlace;

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Town getTown() {
        return town;
    }

    public void setTown(Town town) {
        this.town = town;
    }

    public District getDistrict() {
        return district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }

    public Street getStreet() {
        return street;
    }

    public void setStreet(Street street) {
        this.street = street;
    }
    
    

    public ShowPlace getShowPlace() {
        return showPlace;
    }

    public void setShowPlace(ShowPlace showPlace) {
        this.showPlace = showPlace;
    }

    @Override
    public String toString() {
        return "Address{" + "country=" + country + ", town=" + town + ", district=" + district + ", street=" + street + ", showPlace=" + showPlace + '}';
    }

    
    
}
