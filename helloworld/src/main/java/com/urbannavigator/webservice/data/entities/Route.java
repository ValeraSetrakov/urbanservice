package com.urbannavigator.webservice.data.entities;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

@Entity
public class Route extends TypeObject {
    
    @OneToMany(cascade = CascadeType.ALL)
    private List<RoutePoint> routePoints;

    public Route() {
    }

    public List<RoutePoint> getRoutePoints() {
        return routePoints;
    }

    public void setRoutePoints(List<RoutePoint> routePoints) {
        this.routePoints = routePoints;
    }

    @Override
    public String toString() {
        return "Route{" + "routePoints=" + routePoints + '}';
    }
}
