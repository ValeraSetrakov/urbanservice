
package com.urbannavigator.webservice.data.entities;


public class Coordinate {
    private double longitude;
    private double latitude;

    public Coordinate(double latitude, double longitude) {
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public Coordinate() {
    }
    
    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    @Override
    public String toString() {
        return "Coordinate{" + "longitude=" + longitude + ", latitude=" + latitude + '}';
    }
}
