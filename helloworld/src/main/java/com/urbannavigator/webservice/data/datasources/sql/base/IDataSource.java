package com.urbannavigator.webservice.data.datasources.sql.base;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.hibernate.Session;

public interface IDataSource <Parent> {

    /**
     * Данный метод производик добавление элемента в базу данных
     * @param item - добавляемый элемент
     */
    void insert (Parent item) throws DataSourceException;
    /**
     * Данный метод производик добавление массива элементов в базу данных
     * @param items - массив добавляемых элементов
     */
    void insert (Parent[] items) throws DataSourceException;
    /**
     * Данный метод производик добавление множества элементов в базу данных
     * @param items - множество добавляемых элементов
     */
    void insertAll (Collection<Parent> items) throws DataSourceException;
    /**
     * Данный метод производик обновление элемента в базе данных
     * @param item - обновляемый элемент
     */
    void update (Parent item) throws DataSourceException;
    /**
     * Данный метод производик удаление элемента из базы данных
     * @param item - удаляемый элемент 
     */
    void delete (Parent item) throws DataSourceException;
    /**
     * Данный метод производик удаление элемента из базы данных по его id
     * @param <T> - тип сущности, которую нужно удалить
     * @param id - идентификатор сущности
     * @param c - объекти типа Class, нужен для первоначальной выборки 
     * объекта, который нужно удалить по его id
     */
    <T extends Parent> void deleteItemById (long id, Class<T> c) throws DataSourceException;
    /**
     * Данный метод производик выборку элемента из базы данных по его id
     * @param <T> - тип сущности, которую нужно выбрать
     * @param id - идентификатор сущности
     * @param c - объекти типа Class, нужен для выборки объекта по его id
     * @return 
     */
    <T extends Parent> T selectItem (long id, Class<T> c) throws DataSourceException;
    /**
     * Данный метод производик выборку всех элементов
     * определенного типа из базы данных
     * @param <T> - тип сущности, которую нужно выбрать
     * @param c - объекти типа Class, нужен для определения типа данных, 
     * который нужно выбрать из базы
     * @return - объект, соответствующих переданному идентификатору
     */
    <T extends Parent> List<T> selectAllItems (Class<T> c) throws DataSourceException;
    /**
     * Данный метод производик выборку всех элементов
     * определенного типа из базы данных исходя из строки запроса и переданных 
     * для нее параметров
     * @param <T> - тип сущности, которую нужно выбрать
     * @param query - строка запроса к базе данных
     * @param params - параметры, необходимые для строки запроса
     * @return - список объектов, соответствующих запросу и параметрам
     */
    <T extends Parent> List<T> selectItem (String query, Map<String, Object> params) throws DataSourceException;
    /**
     * Метод для выполнения серии запросов к базе данных
     * @param transaction - данный объект содержит список обращений, необходимых для 
     * выполнения в единой транзакции
     */
    void exec (Transaction transaction) throws DataSourceException;
    
    /**
     * Данный интерфейс представляет возможность выполнения списка обращений 
     * к базе данных в контексте одной сессии через IDataSource
     */
    public interface Transaction {
        public void exec (Session session);
    }
}
