
package com.urbannavigator.webservice.data.entities;

import javax.persistence.Entity;

@Entity
public class Description extends Content{
    
    private String summary;

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    @Override
    public String getValue() {
        return getSummary();
    }

    @Override
    public void setValue(String value) {
        this.summary = value;
    }

    @Override
    public String toString() {
        return "Description{" + "summary=" + summary + '}';
    }
}
