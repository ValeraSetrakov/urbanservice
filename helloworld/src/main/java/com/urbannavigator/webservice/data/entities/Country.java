
package com.urbannavigator.webservice.data.entities;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

@Entity
public class Country extends Locality{
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "country")
    List<Town> towns;

    public Country() {
    }
    
    public List<Town> getTowns() {
        return towns;
    }

    public void setTowns(List<Town> towns) {
        this.towns = towns;
    }

    @Override
    public String toString() {
        return "Country{" + "towns=" + towns + '}';
    }
}
