package com.urbannavigator.webservice.data.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class RoutePoint extends ObjectWithContent {
    
    @ManyToOne
    private Route route;

    @ManyToOne
    private Address address;

    public Route getRoute() {
        return route;
    }

    public void setRoute(Route route) {
        this.route = route;
    }
    
    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "RoutePoint{" + "route=" + route + ", address=" + address + '}';
    }
}
