
package com.urbannavigator.webservice.data.repository;

import com.urbannavigator.webservice.base.TypeLocality;
import com.urbannavigator.webservice.domain.place.IPlaceRepository;
import com.urbannavigator.webservice.data.entities.Country;
import com.urbannavigator.webservice.data.entities.District;
import com.urbannavigator.webservice.data.entities.Locality;
import com.urbannavigator.webservice.data.entities.ShowPlace;
import com.urbannavigator.webservice.data.entities.Street;
import com.urbannavigator.webservice.data.entities.Town;
import com.urbannavigator.webservice.data.datasources.sql.base.DataSourceException;
import com.urbannavigator.webservice.data.datasources.sql.base.IDataSource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class PlaceRepository extends AddressRepository implements IPlaceRepository{

    public PlaceRepository(IDataSource iDataSource) {
        super(iDataSource);
    }
    
    public <T extends Locality> List<T> getAllLocalityFromLocalityById (long parentLocalityId,
            TypeLocality parentTypeLocality,
            TypeLocality childTypeLocality) throws DataSourceException{
        String fromTableName;
        String paramName;
        switch (parentTypeLocality) {
            case TypeCountry: {
                paramName = "country_id";
                break;
            }
            case TypeTown: {
                paramName = "town_id";
                break;
            }
            case TypeDistrict: {
                paramName = "district_id";
                break;
            }
            case TypeStreet: {
                paramName = "street_id";
                break;
            }
            default:throw new DataSourceException("Wrong type of locality");
        }
        switch (childTypeLocality) {
            case TypeTown: {
                fromTableName = "country_id";
                break;
            }
            case TypeDistrict: {
                fromTableName = "town_id";
                break;
            }
            case TypeStreet: {
                fromTableName = "district_id";
                break;
            }
            case TypeShowPlace: {
                fromTableName = "street_id";
                break;
            }
            default:throw new DataSourceException("Wrong type of locality");
        }
        String query = String.format("FROM %s WHERE %s=:%s", fromTableName, paramName, paramName);
        Map<String, Object> params = new HashMap<>(1);
        params.put(paramName, parentLocalityId);
        return iDataSource.selectItem(query, params);
    }

    @Override
    public List<Country> getAllCountries() throws DataSourceException {
        List<Country> countries = iDataSource.selectAllItems(Country.class);
        return countries;
    }

    @Override
    public List<Town> getAllTownsByCountryId(long id) throws DataSourceException {
        List<Town> localities = getAllLocalityFromLocalityById(id, 
                TypeLocality.TypeCountry, 
                TypeLocality.TypeTown);
        return localities;
    }
    
    @Override
    public List<District> getAllDistrictsByTownId(long id) throws DataSourceException {
        List<District> localities = getAllLocalityFromLocalityById(id, 
                TypeLocality.TypeTown, 
                TypeLocality.TypeDistrict);
        return localities;
    }

    @Override
    public List<Street> getAllStreetsByDistrictId(long id) throws DataSourceException {
        List<Street> localities = getAllLocalityFromLocalityById(id, 
                TypeLocality.TypeDistrict, 
                TypeLocality.TypeStreet);
        return localities;
    }

    @Override
    public List<ShowPlace> getAllShowPlacesByStreetId(long id) throws DataSourceException {
        List<ShowPlace> localities = getAllLocalityFromLocalityById(id, 
                TypeLocality.TypeStreet, 
                TypeLocality.TypeShowPlace);
        return localities;
    }
    
}
