package com.urbannavigator.webservice.data.repository;

import com.urbannavigator.webservice.domain.base.IBaseRepository;
import com.urbannavigator.webservice.domain.entity.Response;
import com.urbannavigator.webservice.data.datasources.sql.base.DataSourceException;
import com.urbannavigator.webservice.data.datasources.sql.base.IDataSource;
import com.urbannavigator.webservice.data.entities.DataBaseObject;
import java.util.List;


public class BaseRepository implements IBaseRepository{
    
    protected final IDataSource<DataBaseObject> iDataSource;

    public BaseRepository(final IDataSource iDataSource) {
        this.iDataSource = iDataSource;
    }
    
    public void checkReturnList (final List list, final Class cl) throws DataSourceException {
        if (list == null || list.size() <= 0) throw new DataSourceException(cl.getSimpleName() + " not found objects");
    }
    
    public void checkReturnObject (final Object object) throws DataSourceException {
        if (object == null) throw new DataSourceException(object.getClass().getSimpleName() + " not found objects");
    }

    @Override
    public void saveObject(DataBaseObject dataBaseObject) throws DataSourceException {
        iDataSource.insert(dataBaseObject);
    }
    
    
}
