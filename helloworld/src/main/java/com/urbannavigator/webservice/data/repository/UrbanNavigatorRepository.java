
package com.urbannavigator.webservice.data.repository;

import com.urbannavigator.webservice.data.datasources.sql.base.DataSourceException;
import com.urbannavigator.webservice.data.datasources.sql.base.IDataSource;
import com.urbannavigator.webservice.data.entities.Address;
import com.urbannavigator.webservice.data.entities.Route;
import com.urbannavigator.webservice.data.entities.RoutePoint;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 */
public class UrbanNavigatorRepository extends BaseRepository {

    public UrbanNavigatorRepository(final IDataSource iDataSource) {
        super(iDataSource);
    }
    
    public List<RoutePoint> selectRoutePointsByAddress (final Address address)
        throws DataSourceException {
        String address_id = "address_id";
        String query = String.format("FROM RoutePoint WHERE %s = :%s", address_id, address_id) ;
        Map<String, Object> params = new HashMap<>(1);
        params.put(address_id, address.getId());
        return iDataSource.selectItem(query, params);
    }
    
    
    public List<Route> selectRoutesByAddress (final Address address)
        throws DataSourceException {
        String address_id = "address_id";
        String query = String.format("FROM RoutePoint WHERE %s = :%s", address_id, address_id) ;
        Map<String, Object> params = new HashMap<>(1);
        params.put(address_id, address.getId());
        return iDataSource.selectItem(query, params);
    }
    
    public List<Route> selectRoutesByRoutePoint (final RoutePoint routePoint)
        throws DataSourceException {
        String route_id = "route.id";
        String route_point_id = "routepoint.id";
        String route_route_point_route_point_id = "route_routepoint.routePoints_id";
        String route_route_point_route_id = "route_routepoint.Route_id";
        String query = String.format("FROM Route Join Route_RoutePoint on %s = %s where %s = :%l", route_id, route_route_point_route_id, route_route_point_route_point_id, routePoint.getId()) ;
        Map<String, Object> params = new HashMap<>(1);
        params.put(route_route_point_route_point_id, routePoint.getId());
        return iDataSource.selectItem(query, params);
    }
}
