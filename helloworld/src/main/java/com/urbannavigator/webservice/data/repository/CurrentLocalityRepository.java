
package com.urbannavigator.webservice.data.repository;

import com.urbannavigator.webservice.domain.currentlocality.ICurrentLocalityRepository;
import com.urbannavigator.webservice.data.datasources.sql.base.IDataSource;


public class CurrentLocalityRepository extends PlaceRepository implements ICurrentLocalityRepository{

    public CurrentLocalityRepository(IDataSource iDataSource) {
        super(iDataSource);
    }
    
}
