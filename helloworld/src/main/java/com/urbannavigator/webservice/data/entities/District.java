package com.urbannavigator.webservice.data.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class District extends Locality {
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JsonIgnore
    private Town town;
    
    @OneToMany(mappedBy = "district", cascade = CascadeType.ALL)
    private List<Street> streets;

    public Town getTown() {
        return town;
    }

    public void setTown(Town town) {
        this.town = town;
    }
    
    public List<Street> getStreets() {
        return streets;
    }

    public void setStreets(List<Street> streets) {
        this.streets = streets;
    }

    @Override
    public String toString() {
        return "Area{" + "town=" + town + ", streets=" + streets + '}';
    }
    
}
