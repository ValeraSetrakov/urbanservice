
package com.urbannavigator.webservice.data.entities;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Locality extends GeoObject{
    
    @Embedded
    private Bound bound;

    public Locality() {
    }

    public Locality(Bound viewPort) {
        this.bound = viewPort;
    }
    
    public Bound getBound() {
        return bound;
    }

    public void setBound(Bound bound) {
        this.bound = bound;
    }

    @Override
    public String toString() {
        return "Locality{" + "viewPort=" + bound + '}';
    }
    
}
