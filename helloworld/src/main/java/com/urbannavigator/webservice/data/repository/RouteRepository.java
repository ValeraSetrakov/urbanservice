
package com.urbannavigator.webservice.data.repository;

import com.urbannavigator.webservice.base.TypeLocality;
import com.urbannavigator.webservice.domain.route.IRouteRepository;
import com.urbannavigator.webservice.data.entities.Address;
import com.urbannavigator.webservice.data.entities.Route;
import com.urbannavigator.webservice.data.datasources.sql.base.DataSourceException;
import com.urbannavigator.webservice.data.datasources.sql.base.IDataSource;
import com.urbannavigator.webservice.data.entities.RoutePoint;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class RouteRepository extends CurrentLocalityRepository implements IRouteRepository{

    public RouteRepository(IDataSource iDataSource) {
        super(iDataSource);
    }
    
    public Collection<Route> getRoutesByLocalityId (long id, TypeLocality typeLocality) throws DataSourceException {
        List<Address> addresses = getAddressByLocalityId(id, typeLocality);
        Set<Route> routes = new HashSet<>();
        addresses.forEach(address -> {
            try {
                routes.addAll(getRoutesByAddressId(address.getId()));
            } catch (DataSourceException e) {
                e.printStackTrace();
            }
        });
        return routes;
    }
    
    public Collection<Route> getRoutesByAddressId (long id) throws DataSourceException{
        String paramName = "address_id";
        String query = String.format("FROM RoutePoint WHERE %s=:%s", 
                paramName, paramName);
        HashMap<String, Object> params = new HashMap<>(1);
        params.put(paramName, id);
        List<RoutePoint> routePoints = iDataSource.selectItem(query, params);
        Collection<Route> routes = new HashSet<>();
        routePoints.forEach(routePoint -> routes.add(routePoint.getRoute()));
        return routes;
    }
    
    @Override
    public Collection<Route> getRoutesByCountry(long id) throws DataSourceException {
        return getRoutesByLocalityId(id, TypeLocality.TypeCountry);
    }

    @Override
    public Collection<Route> getRoutesByTown(long id) throws DataSourceException {
        return getRoutesByLocalityId(id, TypeLocality.TypeTown);
    }

    @Override
    public Collection<Route> getRoutesByDistrict(long id) throws DataSourceException {
        return getRoutesByLocalityId(id, TypeLocality.TypeDistrict);
    }

    @Override
    public Collection<Route> getRoutesByStreet(long id) throws DataSourceException {
        return getRoutesByLocalityId(id, TypeLocality.TypeStreet);
    }

    @Override
    public Collection<Route> getRoutesByShowPlace(long id) throws DataSourceException {
        return getRoutesByLocalityId(id, TypeLocality.TypeShowPlace);
    }

    @Override
    public void saveRoute(Route route) throws DataSourceException {
        iDataSource.insert(route);
    }

}
