package com.urbannavigator.webservice.data.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Street extends Locality {
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JsonIgnore
    private District district;
    
    @OneToMany(mappedBy = "street", cascade = CascadeType.ALL)
    private List<ShowPlace> showPlaces;
    
    public Street() {
    }   

    public District getDistrict() {
        return district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }

    public List<ShowPlace> getShowPlaces() {
        return showPlaces;
    }

    public void setShowPlaces(List<ShowPlace> showPlaces) {
        this.showPlaces = showPlaces;
    }

    @Override
    public String toString() {
        return "Street{" + "district=" + district + ", streetObjects=" + showPlaces + '}';
    }
    
}
