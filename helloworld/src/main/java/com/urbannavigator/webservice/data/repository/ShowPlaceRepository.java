
package com.urbannavigator.webservice.data.repository;

import com.urbannavigator.webservice.base.TypeLocality;
import com.urbannavigator.webservice.domain.showplace.IShowPlaceRepository;
import com.urbannavigator.webservice.data.datasources.sql.base.DataSourceException;
import com.urbannavigator.webservice.data.datasources.sql.base.IDataSource;
import com.urbannavigator.webservice.data.entities.Address;
import com.urbannavigator.webservice.data.entities.ShowPlace;
import java.util.ArrayList;
import java.util.List;

public class ShowPlaceRepository extends CurrentLocalityRepository implements IShowPlaceRepository{

    public ShowPlaceRepository(IDataSource iDataSource) {
        super(iDataSource);
    }
    
    

    public List<ShowPlace> getShowPlacesByLocalityId (long id, TypeLocality typeLocality) throws DataSourceException {
        List<Address> addresses = getAddressByLocalityId(id, typeLocality);
        List<ShowPlace> showPlaces = new ArrayList<>(addresses.size());
        addresses.forEach(address -> {
            ShowPlace showPlace = address.getShowPlace();
            showPlace.getContents();
            showPlaces.add(showPlace);
        });
        checkReturnList(showPlaces, ShowPlace.class);
        return showPlaces;
    }
    
    @Override
    public List<ShowPlace> getShowPlacesByCountryId (long id) throws DataSourceException{
        return getShowPlacesByLocalityId(id, TypeLocality.TypeCountry);
    }
    
    @Override
    public List<ShowPlace> getShowPlacesByTownId (long id) throws DataSourceException{
        return getShowPlacesByLocalityId(id, TypeLocality.TypeTown);
    }
    
    @Override
    public List<ShowPlace> getShowPlacesByDistrictId (long id) throws DataSourceException{
        return getShowPlacesByLocalityId(id, TypeLocality.TypeDistrict);
    }
    
    @Override
    public List<ShowPlace> getShowPlacesByStreetId (long id) throws DataSourceException{
        return getShowPlacesByLocalityId(id, TypeLocality.TypeStreet);
    }
  
    @Override
    public ShowPlace getShowPlaceById (long id) throws DataSourceException{
        return getShowPlacesByLocalityId(id, TypeLocality.TypeShowPlace).get(0);
    }
    
}
