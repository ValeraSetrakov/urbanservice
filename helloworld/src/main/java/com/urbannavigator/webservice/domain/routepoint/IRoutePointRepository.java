
package com.urbannavigator.webservice.domain.routepoint;

import com.urbannavigator.webservice.base.TypeLocality;
import com.urbannavigator.webservice.domain.base.IBaseRepository;
import com.urbannavigator.webservice.data.datasources.sql.base.DataSourceException;
import com.urbannavigator.webservice.data.entities.RoutePoint;
import java.util.List;


public interface IRoutePointRepository extends IBaseRepository{
    RoutePoint getRoutePointById (long id) throws DataSourceException;
    List<RoutePoint> getRoutePointsByLocalityId (long id, TypeLocality typeLocality) throws DataSourceException;
    List<RoutePoint> getRoutePointsByAddressId (long id) throws DataSourceException;
    void saveRoutePoint (RoutePoint routePoint) throws DataSourceException;
}
