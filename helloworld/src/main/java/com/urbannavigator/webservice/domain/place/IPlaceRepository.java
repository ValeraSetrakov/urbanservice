/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.urbannavigator.webservice.domain.place;

import com.urbannavigator.webservice.domain.base.IBaseRepository;
import com.urbannavigator.webservice.data.entities.Country;
import com.urbannavigator.webservice.data.entities.District;
import com.urbannavigator.webservice.data.entities.ShowPlace;
import com.urbannavigator.webservice.data.entities.Street;
import com.urbannavigator.webservice.data.entities.Town;
import com.urbannavigator.webservice.data.datasources.sql.base.DataSourceException;
import java.util.List;

public interface IPlaceRepository extends IBaseRepository{
    List<Country> getAllCountries () throws DataSourceException;
    List<Town> getAllTownsByCountryId (long id) throws DataSourceException;
    List<District> getAllDistrictsByTownId (long id) throws DataSourceException;
    List<Street> getAllStreetsByDistrictId (long id) throws DataSourceException;
    List<ShowPlace> getAllShowPlacesByStreetId (long id) throws DataSourceException;
}
