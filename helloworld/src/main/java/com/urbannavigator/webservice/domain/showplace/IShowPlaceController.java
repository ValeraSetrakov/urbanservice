package com.urbannavigator.webservice.domain.showplace;

import com.urbannavigator.webservice.data.datasources.sql.base.DataSourceException;
import com.urbannavigator.webservice.domain.base.IBaseController;
import com.urbannavigator.webservice.data.entities.ShowPlace;
import java.util.List;

public interface IShowPlaceController extends IBaseController{
    List<ShowPlace> getShowPlacesByCurrentPlace(double lan, double lon) throws DataSourceException;
    List<ShowPlace> getShowPlacesByCountry(long id) throws DataSourceException;
    List<ShowPlace> getShowPlacesByTown(long id) throws DataSourceException;
    List<ShowPlace> getShowPlacesByDistrict(long id) throws DataSourceException;
    List<ShowPlace> getShowPlacesByStreet(long id) throws DataSourceException;
}
