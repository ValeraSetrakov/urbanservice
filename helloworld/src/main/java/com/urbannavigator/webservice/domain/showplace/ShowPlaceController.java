
package com.urbannavigator.webservice.domain.showplace;

import com.urbannavigator.webservice.base.TypeLocality;
import com.urbannavigator.webservice.domain.currentlocality.CurrentLocalityController;
import com.urbannavigator.webservice.domain.entity.Response;
import com.urbannavigator.webservice.data.entities.ShowPlace;
import com.urbannavigator.webservice.data.entities.Street;
import com.urbannavigator.webservice.data.datasources.sql.base.DataSourceException;
import java.util.List;


public class ShowPlaceController extends CurrentLocalityController <IShowPlaceRepository> implements IShowPlaceController{

    public ShowPlaceController(IShowPlaceRepository repository) {
        super(repository);
    }
    
    public List<ShowPlace> getShowPlacesByLocalityId (long id, TypeLocality typeLocality) throws DataSourceException{
        switch (typeLocality) {
            case TypeCountry: {
                return repository.getShowPlacesByCountryId(id);
            }
            case TypeTown: {
                return repository.getShowPlacesByTownId(id);
            }
            case TypeDistrict: {
                return repository.getShowPlacesByDistrictId(id);
            }
            case TypeStreet: {
                return repository.getShowPlacesByStreetId(id);
            }
            default: throw new DataSourceException("Wrong locality type");
        }
    }

    @Override
    public List<ShowPlace> getShowPlacesByCurrentPlace(double lan, double lon) throws DataSourceException{
        Street currentStreet = selectCurrentStreet(lan, lon);
        return getShowPlacesByStreet(currentStreet.getId());
    }

    @Override
    public List<ShowPlace> getShowPlacesByCountry(long id) throws DataSourceException{
        return getShowPlacesByLocalityId(id, TypeLocality.TypeCountry);
    }

    @Override
    public List<ShowPlace> getShowPlacesByTown(long id) throws DataSourceException{
        return getShowPlacesByLocalityId(id, TypeLocality.TypeTown);
    }

    @Override
    public List<ShowPlace> getShowPlacesByDistrict(long id) throws DataSourceException{
        return getShowPlacesByLocalityId(id, TypeLocality.TypeDistrict);
    }

    @Override
    public List<ShowPlace> getShowPlacesByStreet(long id) throws DataSourceException{
        return getShowPlacesByLocalityId(id, TypeLocality.TypeStreet);
    }
}
