
package com.urbannavigator.webservice.domain.base;

import com.urbannavigator.webservice.domain.entity.Response;
import com.urbannavigator.webservice.data.datasources.sql.base.DataSourceException;
import com.urbannavigator.webservice.data.entities.DataBaseObject;


public class BaseController <REPOSITORY extends IBaseRepository> implements IBaseController{
    protected REPOSITORY repository;

    public BaseController(REPOSITORY repository) {
        this.repository = repository;
    }

    @Override
    public Response saveObject(DataBaseObject dataBaseObject) {
        try {
            repository.saveObject(dataBaseObject);
            return new Response("OK");
        } catch (DataSourceException e) {
            return new Response(e);
        }
    }
}
