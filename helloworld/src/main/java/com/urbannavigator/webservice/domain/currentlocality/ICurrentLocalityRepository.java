/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.urbannavigator.webservice.domain.currentlocality;

import com.urbannavigator.webservice.data.entities.Country;
import com.urbannavigator.webservice.data.datasources.sql.base.DataSourceException;
import java.util.List;
import com.urbannavigator.webservice.domain.place.IPlaceRepository;

/**
 *
 * @author valera
 */
public interface ICurrentLocalityRepository extends IPlaceRepository{
    List<Country> getAllCountries() throws DataSourceException;
}
