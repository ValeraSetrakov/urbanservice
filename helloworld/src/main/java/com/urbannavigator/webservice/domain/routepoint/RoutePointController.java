
package com.urbannavigator.webservice.domain.routepoint;

import com.urbannavigator.webservice.base.TypeLocality;
import com.urbannavigator.webservice.domain.base.BaseController;
import com.urbannavigator.webservice.domain.entity.Response;
import com.urbannavigator.webservice.data.datasources.sql.base.DataSourceException;
import com.urbannavigator.webservice.data.entities.RoutePoint;
import java.util.List;


public class RoutePointController extends BaseController<IRoutePointRepository>implements IRoutePointController{

    public RoutePointController(IRoutePointRepository repository) {
        super(repository);
    }

    @Override
    public Response<RoutePoint> getRoutePointById(long id) {
        try {
            return new Response<>(repository.getRoutePointById(id));
        } catch (DataSourceException e) {
            return new Response(e);
        }
    }

    @Override
    public Response<List<RoutePoint>> getRoutePointsByLocalityId(long id, TypeLocality typeLocality) {
        try {
            return new Response<>(repository.getRoutePointsByLocalityId(id, typeLocality));
        } catch (DataSourceException e) {
            return new Response(e);
        }
    }

    @Override
    public Response<List<RoutePoint>> getRoutePointsByAddressId(long id) {
        try {
            return new Response<>(repository.getRoutePointsByAddressId(id));
        } catch (DataSourceException e) {
            return new Response(e);
        }
    }

    @Override
    public Response saveRoutePoint(RoutePoint routePoint) {
        try {
            repository.saveRoutePoint(routePoint);
            return new Response("OK");
        } catch (DataSourceException e) {
            return new Response(e);
        }
    }
    
    
}
