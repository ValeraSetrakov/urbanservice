/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.urbannavigator.webservice.domain.base;

import com.urbannavigator.webservice.domain.entity.Response;
import com.urbannavigator.webservice.data.datasources.sql.base.DataSourceException;
import com.urbannavigator.webservice.data.entities.DataBaseObject;

/**
 *
 * @author valera
 */
public interface IBaseRepository {
    void saveObject (DataBaseObject dataBaseObject)throws DataSourceException;
}
