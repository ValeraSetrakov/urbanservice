
package com.urbannavigator.webservice.domain.route;

import com.urbannavigator.webservice.base.TypeLocality;
import com.urbannavigator.webservice.domain.currentlocality.CurrentLocalityController;
import com.urbannavigator.webservice.domain.entity.Response;
import com.urbannavigator.webservice.data.entities.Route;
import com.urbannavigator.webservice.data.entities.Street;
import com.urbannavigator.webservice.data.datasources.sql.base.DataSourceException;
import java.util.Collection;

public class RouteController extends CurrentLocalityController<IRouteRepository> implements IRouteControler{

    public RouteController(IRouteRepository repository) {
        super(repository);
    }
    
    public Collection<Route> getRoutesByLocalityId (long id, TypeLocality typeLocality) 
            throws DataSourceException {
        switch (typeLocality) {
            case TypeCountry: {
                return repository.getRoutesByCountry(id);
            }
            case TypeTown: {
                return repository.getRoutesByTown(id);
            }
            case TypeDistrict: {
                return repository.getRoutesByDistrict(id);
            }
            case TypeStreet: {
                return repository.getRoutesByStreet(id);
            }
            case TypeShowPlace: {
                return repository.getRoutesByShowPlace(id);
            }
            default: throw new DataSourceException("Wrong locality type");
        }
    }
    
    @Override
    public Collection<Route> getRoutesByCurrentPlace(double lan, double lon) 
        throws DataSourceException {
        Street currentStreet = selectCurrentStreet(lan, lon);
        return getRoutesByStreet(currentStreet.getId());
    }

    @Override
    public Collection<Route> getRoutesByCountry(long id) 
        throws DataSourceException {
        return getRoutesByLocalityId(id, TypeLocality.TypeCountry);
    }

    @Override
    public Collection<Route> getRoutesByTown(long id) 
        throws DataSourceException {
        return getRoutesByLocalityId(id, TypeLocality.TypeTown);
    }

    @Override
    public Collection<Route> getRoutesByDistrict(long id) 
        throws DataSourceException {
        return getRoutesByLocalityId(id, TypeLocality.TypeDistrict);
    }

    @Override
    public Collection<Route> getRoutesByStreet(long id) 
        throws DataSourceException {
        return getRoutesByLocalityId(id, TypeLocality.TypeStreet);
    }
    
    @Override
    public Collection<Route> getRoutesByShowPlace(long id) 
        throws DataSourceException {
        return getRoutesByLocalityId(id, TypeLocality.TypeShowPlace);
    }
    
}
