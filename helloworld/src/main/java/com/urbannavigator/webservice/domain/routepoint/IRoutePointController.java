
package com.urbannavigator.webservice.domain.routepoint;

import com.urbannavigator.webservice.base.TypeLocality;
import com.urbannavigator.webservice.domain.entity.Response;
import com.urbannavigator.webservice.data.datasources.sql.base.DataSourceException;
import com.urbannavigator.webservice.data.entities.RoutePoint;
import java.util.List;


public interface IRoutePointController {
    Response<RoutePoint> getRoutePointById (long id);
    Response<List<RoutePoint>> getRoutePointsByLocalityId (long id, TypeLocality typeLocality);
    Response<List<RoutePoint>> getRoutePointsByAddressId (long id) ;
    Response saveRoutePoint (RoutePoint routePoint);
}
