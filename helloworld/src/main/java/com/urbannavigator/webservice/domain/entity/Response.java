
package com.urbannavigator.webservice.domain.entity;


public class Response <T> {
    private T t;
    private String message;

    public Response(T t, String message) {
        this.t = t;
        this.message = message;
    }

    public Response(T t) {
        this.t = t;
    }
    
    public Response(String message) {
        this.message = message;
    }
    
    public T getT() {
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
    public static class Builder {
        public static <Type> Response<Type> createSuccessFrom(Type t) {
            return new Response<>(t, "OK");
        }
    }
    
}
