
package com.urbannavigator.webservice.domain.currentlocality;

import com.urbannavigator.webservice.domain.entity.Response;
import com.urbannavigator.webservice.domain.place.PlaceController;
import com.urbannavigator.webservice.data.datasources.sql.base.DataSourceException;
import com.urbannavigator.webservice.data.entities.District;
import com.urbannavigator.webservice.data.entities.Coordinate;
import com.urbannavigator.webservice.data.entities.Country;
import com.urbannavigator.webservice.data.entities.Locality;
import com.urbannavigator.webservice.data.entities.Street;
import com.urbannavigator.webservice.data.entities.ShowPlace;
import com.urbannavigator.webservice.data.entities.Town;
import com.urbannavigator.webservice.data.entities.Bound;
import java.util.List;


public class CurrentLocalityController <REPOSITORY extends ICurrentLocalityRepository> extends PlaceController<REPOSITORY>{
        
    public CurrentLocalityController(REPOSITORY repository) {
        super(repository);
    }
    
    public Country selectCurrentCountry (final double lat, final double lon) throws DataSourceException {
        List<Country> countries = repository.getAllCountries();
        if (countries != null && countries.size() > 0) {
            return countries.stream()
                .filter(locality -> checkIsCurrentLocality(locality, lat, lon))
                .findFirst()
                .orElseThrow(() -> new DataSourceException("No such country"));
        } else {
            throw new DataSourceException("No such country");
        }       
    }
    
    public Town selectCurrentTown (final Country currentCountry, final double lat, final double lon) throws DataSourceException {
        List<Town> towns = currentCountry.getTowns();
        if (towns != null && towns.size() > 0) {
            return towns.stream()
                .filter(locality -> checkIsCurrentLocality(locality, lat, lon))
                .findFirst()
                .orElseThrow(() -> new DataSourceException("No such town"));
        } else {
            throw new DataSourceException("No such town");
        }
    }
    
    public District selectCurrentDistrict (final Town town, final double lat, final double lon) throws DataSourceException {
        List<District> districts = town.getDistricts();
        if (districts != null && districts.size() > 0) {
            return districts.stream()
                .filter(locality -> checkIsCurrentLocality(locality, lat, lon))
                .findFirst()
                .orElseThrow(() -> new DataSourceException("No such district"));
        } else {
            throw new DataSourceException("No such district");
        }
    }
    
    public Street selectCurrentStreet (final District district, final double lat, final double lon) throws DataSourceException {
        List<Street> streets = district.getStreets();
        if (streets != null && streets.size() > 0) {
            return streets.stream()
                .filter(locality -> checkIsCurrentLocality(locality, lat, lon))
                .findFirst()
                .orElseThrow(() -> new DataSourceException("No such street"));
        } else {
            throw new DataSourceException("No such street");
        }
    }
    
    public Response<List<ShowPlace>> selectNearShowPlace (final double lat, final double lon) {
        try{
            return new Response<>(selectCurrentStreet(selectCurrentDistrict(selectCurrentTown(selectCurrentCountry(lat, lon), lat, lon),lat, lon),lat, lon).getShowPlaces());
        } catch(DataSourceException err) {
            return new Response<>(err.getMessage());
        }
    }
    
    public Town selectCurrentTown (final double lat, final double lon) throws DataSourceException {
        Country currentCountry = selectCurrentCountry(lat, lon);
        Town currentTown = selectCurrentTown(currentCountry, lat, lon);
        return currentTown;
    }
    
    public District selectCurrentDistrict (final double lat, final double lon) throws DataSourceException {
        Country currentCountry = selectCurrentCountry(lat, lon);
        Town currentTown = selectCurrentTown(currentCountry, lat, lon);
        District currentDistrict = selectCurrentDistrict(currentTown, lat, lon);
        return currentDistrict;
    }
    
    public Street selectCurrentStreet (final double lat, final double lon) throws DataSourceException {
        Country currentCountry = selectCurrentCountry(lat, lon);
        Town currentTown = selectCurrentTown(currentCountry, lat, lon);
        District currentDistrict = selectCurrentDistrict(currentTown, lat, lon);
        Street currentStreet = selectCurrentStreet(currentDistrict, lat, lon);
        return currentStreet;
    }
    
    
    
    /**
     * check if locality contains user's coordinate 
     * @param locality
     * @param lat
     * @param lon
     * @return true if locality contain user's cooridinate otherwise false
     */
    protected boolean checkIsCurrentLocality (final Locality locality, final double lat, final double lon) {
        Bound localityViewPort = locality.getBound();
        Coordinate currentLocalityLeftTopCoordinate = localityViewPort.getNortheast();
        Coordinate currentLocalityRightBottomCoordinate = localityViewPort.getSouthwest();
        double left = currentLocalityLeftTopCoordinate.getLatitude();
        double top = currentLocalityLeftTopCoordinate.getLongitude();
        double rigth = currentLocalityRightBottomCoordinate.getLatitude();
        double bottom = currentLocalityRightBottomCoordinate.getLongitude();
        boolean result = 
                lat > Math.min(left, rigth)? 
                lat < Math.max(left, rigth)? 
                lon > Math.min(top, bottom)? lon < Math.max(top, bottom): false
                : false 
                : false;
        return result;
    }
}
