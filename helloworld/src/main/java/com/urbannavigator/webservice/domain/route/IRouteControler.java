package com.urbannavigator.webservice.domain.route;

import com.urbannavigator.webservice.data.datasources.sql.base.DataSourceException;
import com.urbannavigator.webservice.domain.base.IBaseController;
import com.urbannavigator.webservice.domain.entity.Response;
import com.urbannavigator.webservice.data.entities.Route;
import java.util.Collection;
import java.util.List;

public interface IRouteControler extends IBaseController{
    public Collection<Route> getRoutesByCurrentPlace(double lan, double lon) throws DataSourceException;
    public Collection<Route> getRoutesByCountry(long id) throws DataSourceException;
    public Collection<Route> getRoutesByTown(long id) throws DataSourceException;
    public Collection<Route> getRoutesByDistrict(long id) throws DataSourceException;
    public Collection<Route> getRoutesByStreet(long id) throws DataSourceException;
    public Collection<Route> getRoutesByShowPlace(long id) throws DataSourceException;
}
