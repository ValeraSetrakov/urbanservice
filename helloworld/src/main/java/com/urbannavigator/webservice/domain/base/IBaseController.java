
package com.urbannavigator.webservice.domain.base;

import com.urbannavigator.webservice.data.datasources.sql.base.DataSourceException;
import com.urbannavigator.webservice.domain.entity.Response;
import com.urbannavigator.webservice.data.entities.DataBaseObject;


public interface IBaseController {
    Response saveObject (DataBaseObject dataBaseObject) throws DataSourceException;
}
