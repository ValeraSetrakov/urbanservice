/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.urbannavigator.webservice.domain.showplace;

import com.urbannavigator.webservice.domain.currentlocality.ICurrentLocalityRepository;
import com.urbannavigator.webservice.data.entities.ShowPlace;
import com.urbannavigator.webservice.data.datasources.sql.base.DataSourceException;
import java.util.List;

public interface IShowPlaceRepository extends ICurrentLocalityRepository{
    List<ShowPlace> getShowPlacesByCountryId(long id) throws DataSourceException;
    List<ShowPlace> getShowPlacesByTownId(long id) throws DataSourceException;
    List<ShowPlace> getShowPlacesByDistrictId(long id) throws DataSourceException;
    List<ShowPlace> getShowPlacesByStreetId(long id) throws DataSourceException;
    ShowPlace getShowPlaceById(long id) throws DataSourceException;
}
