/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.urbannavigator.webservice.domain.route;

import com.urbannavigator.webservice.domain.currentlocality.ICurrentLocalityRepository;
import com.urbannavigator.webservice.data.entities.Route;
import com.urbannavigator.webservice.data.datasources.sql.base.DataSourceException;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author valera
 */
public interface IRouteRepository extends ICurrentLocalityRepository{
    Collection<Route> getRoutesByCountry(long id) throws DataSourceException;
    Collection<Route> getRoutesByTown(long id) throws DataSourceException;
    Collection<Route> getRoutesByDistrict(long id) throws DataSourceException;
    Collection<Route> getRoutesByStreet(long id) throws DataSourceException;
    Collection<Route> getRoutesByShowPlace(long id) throws DataSourceException;
    void saveRoute (Route route) throws DataSourceException;
}
