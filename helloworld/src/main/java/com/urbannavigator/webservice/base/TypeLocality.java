
package com.urbannavigator.webservice.base;


public enum TypeLocality {
    TypeCountry, TypeTown, TypeDistrict, TypeStreet, TypeShowPlace
}
