
package com.urbannavigator.webservice.util;

import com.urbannavigator.webservice.data.datasources.sql.base.DataSourceCore;
import com.urbannavigator.webservice.data.datasources.sql.base.DataSourceException;
import com.urbannavigator.webservice.data.entities.Address;
import com.urbannavigator.webservice.data.entities.Bound;
import com.urbannavigator.webservice.data.entities.Category;
import com.urbannavigator.webservice.data.entities.Coordinate;
import com.urbannavigator.webservice.data.entities.Country;
import com.urbannavigator.webservice.data.entities.District;
import com.urbannavigator.webservice.data.entities.Locality;
import com.urbannavigator.webservice.data.entities.Route;
import com.urbannavigator.webservice.data.entities.RoutePoint;
import com.urbannavigator.webservice.data.entities.ShowPlace;
import com.urbannavigator.webservice.data.entities.Street;
import com.urbannavigator.webservice.data.entities.Town;
import com.urbannavigator.webservice.data.entities.Type;
import com.urbannavigator.webservice.data.hibernate.HibernateUtil;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;


public class EntityGenerator {
    
    public static int objectId = 0;
    
    public static int countElements = 10;
    
    public static ShowPlace generateShowPlace (
            String name, Coordinate coordinate, Bound bound, Street street
    ) {
        ShowPlace showPlace = new ShowPlace();
        showPlace.setName(name);
        showPlace.setCoordinate(coordinate);
        showPlace.setBound(bound);
        showPlace.setStreet(street);
        return showPlace;
    }
    
    
    public static Street generateStreet (
            String name, Coordinate coordinate, Bound bound, District area
    ) {
        Street street = new Street();
        street.setName(name);
        street.setCoordinate(coordinate);
        street.setBound(bound);
        street.setDistrict(area);
        return street;
    }
    
    public static District generateArea (
            String name, Coordinate coordinate, Bound bound, Town town
    ) {
        District area = new District();
        area.setName(name);
        area.setCoordinate(coordinate);
        area.setBound(bound);
        area.setTown(town);
        return area;
    }
    
    public static Town generateTown (
            String name, Coordinate coordinate, Bound bound, Country country
    ) {
        Town town = new Town();
        town.setName(name);
        town.setCoordinate(coordinate);
        town.setBound(bound);
        town.setCountry(country);
        return town;
    }
    
    public static Country generateCountry (
            String name, Coordinate coordinate, Bound bound
    ) {
        Country country = new Country(); 
        country.setName(name);
        country.setCoordinate(coordinate);
        country.setBound(bound);
        return country;
    }
    
    public static List<ShowPlace> generateShowPlaces (int count, Street street) {
        List<ShowPlace> showPlaces = new ArrayList<>();
        String name = "Street";
        Bound[] newBounds = createBounds(street, count);
        for (int i = 0; i < count; i++) {
            Bound bound = newBounds[i];
            Coordinate centerCoordinate = createCenterCoordinate(bound);
            showPlaces.add(generateShowPlace(name + i, centerCoordinate, bound, street));
        }
        return showPlaces;
    }
    
    public static List<Street> generateStreets (int count, District district) {
        List<Street> streets = new ArrayList<>();
        String name = "Street";
        Bound[] newBounds = createBounds(district, count);
        for (int i = 0; i < count; i++) {
            Bound bound = newBounds[i];
            Coordinate centerCoordinate = createCenterCoordinate(bound);
            streets.add(generateStreet(name + i, centerCoordinate, bound, district));
        }
        return streets;
    }
    
    public static List<District> generateDistricts (int count, Town town) {
        List<District> areas = new ArrayList<>();
        String name = "Area";
        Bound[] newBounds = createBounds(town, count);
        for (int i = 0; i < count; i++) {
            Bound bound = newBounds[i];
            Coordinate centerCoordinate = createCenterCoordinate(bound);
            areas.add(generateArea(name + i, centerCoordinate, bound, town));
        }
        return areas;
    }
    
    public static List<Town> generateTowns (int count, Country country) {
        List<Town> towns = new ArrayList<>();
        String name = "Town";
        Bound[] newBounds = createBounds(country, count);
        for (int i = 0; i < count; i++) {
            Bound bound = newBounds[i];
            Coordinate centerCoordinate = createCenterCoordinate(bound);
            towns.add(generateTown(name + i, centerCoordinate, bound, country));
        }
        return towns;
    }
    
    public static Address createAddress (ShowPlace showPlace) {
        Address address = new Address();
        address.setShowPlace(showPlace);
        address.setStreet(address.getShowPlace().getStreet());
        address.setDistrict(address.getStreet().getDistrict());
        address.setTown(address.getDistrict().getTown());
        address.setCountry(address.getTown().getCountry());
        return address;
    }
    
    public static List<Address> createAddresses(Country country) {
        List<Address> addresses = new ArrayList<>();
        country.getTowns().forEach(town -> {
            town.getDistricts().forEach(district -> {
                district.getStreets().forEach(street -> {
                    street.getShowPlaces().forEach(showPlace -> {
                        addresses.add(createAddress(showPlace));
                    });
                });
            });
        });
        return addresses;
    }
    
    public static Bound[] createBounds (Locality locality, int count) {
        Bound[] bounds = new Bound[count];
        Bound bound = locality.getBound();
        
        Coordinate northeast = bound.getNortheast();
        Coordinate southwest = bound.getSouthwest();
        
        double latn = northeast.getLatitude();
        double lats = southwest.getLatitude();
        
        double lonn = northeast.getLongitude();
        double lons = southwest.getLongitude();
        
        double leftLat = Math.min(latn, lats);
        double rigthLat = Math.max(latn, lats);
        
        double topLon = Math.min(lonn, lons);
        double bottomLon = Math.max(lats, lons);
        
        double diffLat = rigthLat - leftLat;
        double diffLon = bottomLon - topLon;
        
        double diffLatStep = diffLat/count;
        double diffLonStep = diffLon/count;
        
        
        
        for (int i = 0; i < count; i++) {
            double newLatRigth = (i + 1) * diffLatStep;
            double newLatLeft =  i * diffLatStep;
            double newLonBottom = (i + 1) * diffLonStep;
            double newLonTop =  i * diffLonStep;
            Bound newBound = new Bound();
            newBound.setNortheast(new Coordinate(newLatLeft, newLonTop));
            newBound.setSouthwest(new Coordinate(newLatRigth, newLonBottom));
            bounds[i] = newBound;
        }
        return bounds;
    }
    
    public static Coordinate createCenterCoordinate (Bound bound) {
        double latl = bound.getNortheast().getLatitude();
        double latr = bound.getSouthwest().getLatitude();
        
        double lont = bound.getNortheast().getLongitude();
        double lonb = bound.getSouthwest().getLongitude();
        
        double latc = latl + (latr - latl)/2;
        double lonc = lont + (lonb - lont)/2;
        
        Coordinate coourdinate = new Coordinate(latc, lonc);
        return coourdinate;
    }
    
    public static RoutePoint generateRoutePoint (String name, Route route, Address address) {
        RoutePoint routePoint = new RoutePoint();
        routePoint.setName(name);
        routePoint.setAddress(address);
        routePoint.setRoute(route);
        return routePoint;
    }
    
    public static List<RoutePoint> generateRoutePoints (int count, Route route, List<Address> addresses) {
        List<RoutePoint> routePoints = new ArrayList<>(count);
        Random random = new Random(new Date().getTime());
        for (int i = 0; i < count; i++) {
            RoutePoint routePoint = 
                    generateRoutePoint("Route Point " + i, route, addresses.get(random.nextInt(addresses.size())));
            routePoints.add(routePoint);
        }
        return routePoints;
    }
    
    public static Route generateRoute (String name, Category category, Type type) {
        Route route = new Route();
        route.setName(name);
        route.setCategory(category);
        route.setType(type);
        return route;
    }
    
    public static List<Route> generateRoutes (int count, Category category, Type type) {
        List<Route> routePoints = new ArrayList<>(count);
        for (int i = 0; i < count; i++) {
            Route route = generateRoute("Route " + i, category, type);
            routePoints.add(route);
        }
        return routePoints;
    }
    
    
    
    
    
    
    
    
    
    
    
    private static <T extends Locality> T completeLocality (
            T locality,
            String name,
            Coordinate centerCoordinate,
            Bound viewPort
    ) {
        locality.setName(name);
        locality.setCoordinate(centerCoordinate);
        locality.setBound(viewPort);
        return locality;
    }
    
    public static Country createRussia () {
        Country russia = completeLocality(new Country(),
                "Russia",
                new Coordinate(61.52401, 105.318756),
                new Bound(
                new Coordinate(82.1673907, -168.97788)
                , new Coordinate(41.185353, 19.6160999))
        );
        russia.setTowns(Arrays.asList(createRostov(russia)));
        return russia;
    }
    
    public static Town createRostov (Country country) {
        Town rostov = completeLocality(new Town(),
                "Rostov",
                new Coordinate(47.2357137, 39.701505),
                new Bound(
                        new Coordinate(47.36868, 39.8514601)
                        , new Coordinate(47.153337, 39.40453979999999))
        );
        rostov.setDistricts(Arrays.asList(createOktDistrict(rostov), createSovetskiyDistrict(rostov)));
        rostov.setCountry(country);
        return rostov;
    }
    
    public static District createOktDistrict (Town town) {
        District oktArea = new District();
        oktArea.setName("Oktyabr'skiy Rayon");
        oktArea.setCoordinate(new Coordinate(47.2692859, 39.64828370000001));
        oktArea.setBound(new Bound(
                        new Coordinate(47.3207998, 39.7159274)
                        , new Coordinate(47.225769, 39.6140985)));
        oktArea.setStreets(Arrays.asList(createDebaltsevskaya(oktArea)));
        oktArea.setTown(town);
        return oktArea;
    }
    
    public static District createSovetskiyDistrict (Town town) {
        District oktArea = new District();
        oktArea.setName("Sovetskiy Rayon");
        oktArea.setCoordinate(new Coordinate(47.2281247, 39.5799362));
        oktArea.setBound(new Bound(
                        new Coordinate(47.1666751, 39.4045099)
                        , new Coordinate(47.2615247, 39.6688501)));
        oktArea.setStreets(Arrays.asList(createZorge(oktArea)));
        oktArea.setTown(town);
        return oktArea;
    }
    
    public static Street createDebaltsevskaya (District district) {
        Street debaltsevskaya = new Street();
        debaltsevskaya.setName("Debal'tsevskaya");
        debaltsevskaya.setCoordinate(new Coordinate(47.263587, 39.6493081));
        debaltsevskaya.setBound(new Bound(
                        new Coordinate(47.265944, 39.65189669999999)
                        , new Coordinate(47.26047990000001, 39.6456019)));
        debaltsevskaya.setShowPlaces(Arrays.asList(createBuild14(debaltsevskaya)));
        debaltsevskaya.setDistrict(district);
        return debaltsevskaya;
    }
    
    public static Street createZorge (District district) {
        Street debaltsevskaya = new Street();
        debaltsevskaya.setName("Zorge");
        debaltsevskaya.setCoordinate(new Coordinate(47.22065, 39.63179));
        debaltsevskaya.setBound(new Bound(
                        new Coordinate(47.2081259, 39.6240979)
                        , new Coordinate(47.2324264, 39.6369241)));
        debaltsevskaya.setShowPlaces(Arrays.asList(createMyHouse(debaltsevskaya), createMagnite (debaltsevskaya), createFVT(debaltsevskaya)));
        debaltsevskaya.setDistrict(district);
        return debaltsevskaya;
    }
    
    public static ShowPlace createBuild14 (Street street) {
        ShowPlace so = completeLocality(new ShowPlace(),
                "14",
                new Coordinate(47.265944, 39.6456019),
                new Bound(
                        new Coordinate(47.36868, 39.8514601)
                        , new Coordinate(47.153337, 39.40453979999999))
        );
        so.setStreet(street);
        return so;
    }
    
    public static ShowPlace createMyHouse (Street street) {
        ShowPlace so = completeLocality(new ShowPlace(),
                "Home 19",
                new Coordinate(47.2222036, 39.6289289),
                new Bound(
                        new Coordinate(47.2208546, 39.6275799)
                        , new Coordinate(47.2235526, 39.6302779))
        );
        so.setStreet(street);
        return so;
    }
    
    public static ShowPlace createMagnite (Street street) {
        ShowPlace so = completeLocality(new ShowPlace(),
                "Magnite 58/1",
                new Coordinate(47.2238463, 39.6310379),
                new Bound(
                        new Coordinate(47.2224973, 39.6296889)
                        , new Coordinate(47.2251953, 39.6323869))
        );
        so.setStreet(street);
        return so;
    }
    
    public static ShowPlace createFVT (Street street) {
        ShowPlace so = completeLocality(new ShowPlace(),
                "FVT 10",
                new Coordinate(47.2189280, 39.6273539),
                new Bound(
                        new Coordinate(47.2175790, 39.6260049)
                        , new Coordinate(47.2202770, 39.6287029))
        );
        so.setStreet(street);
        return so;
    }
    
    public static void createAndSaveStartData () throws Exception {
//        saveCountry(createRussia());
    }
    
    
    
    
    
    private static DataSourceCore dataSourceCore = new DataSourceCore(HibernateUtil.getSessionFactory());
    
    public static void createAndSaveCategories () throws DataSourceException{
        
        Category category1 = new Category();
        category1.setName("Category 1");
        
        Category category2 = new Category();
        category2.setName("Category 2");
        
        dataSourceCore.insert(category1);
        dataSourceCore.insert(category2);
    }
    
    public static void createAndSaveTypes () throws DataSourceException{
        
        Type type1 = new Type();
        type1.setName("Type 1");
        
        Type type2 = new Type();
        type2.setName("Type 2");
        
        dataSourceCore.insert(type1);
        dataSourceCore.insert(type2);
    }
    
    public static void createAndSaveCountry () throws Exception  {
        int count = 2;
        Bound bound = new Bound(new Coordinate(400, 400), new Coordinate(0,0));
        Coordinate coordinate = createCenterCoordinate(bound);
        Country country = generateCountry("Country 1", coordinate, bound);
        List<Town> towns = generateTowns(count, country);
        country.setTowns(towns);
        towns.forEach(town -> {
            List<District> districts = generateDistricts(count, town);
            town.setDistricts(districts);
            districts.forEach(district -> {
                List<Street> streets = generateStreets(count, district);
                district.setStreets(streets);
                streets.forEach(street -> {
                    List<ShowPlace> showPlaces = generateShowPlaces(count, street);
                    street.setShowPlaces(showPlaces);
                });
            });
        });
        dataSourceCore.insert(country);
    }
    
    public static void createAndSaveAddress () throws Exception {
        Country country = dataSourceCore.selectAllItems(Country.class).get(0);
        List<Address> addresses = createAddresses(country);
        addresses.forEach(address -> {
            try {
                dataSourceCore.insert(address);
            } catch (DataSourceException e) {
                e.printStackTrace();
            }
        });
    } 
    
    public static void createAndSaveRoute () throws Exception {
        List<Category> categorys = dataSourceCore.selectAllItems(Category.class);
        List<Type> types = dataSourceCore.selectAllItems(Type.class);
        int count = 3;
        List<Address> addresses = dataSourceCore.selectAllItems(Address.class);
        for (int i = 0; i < count; i++) {
            Route route = generateRoute("Route " + i, categorys.get(0), types.get(0));
            List<RoutePoint> routePoints = new ArrayList<>(count);
            for (int j = 0; j < count; j++) {
                RoutePoint routePoint = generateRoutePoint("Route Point " + j, route, addresses.get(j));
                routePoints.add(routePoint);
            }
            route.setRoutePoints(routePoints);
            dataSourceCore.insert(route);
        }
//        categorys.forEach(category -> {
//            types.forEach(type -> {
//                List<Route> routes = generateRoutes(count, category, type);
//                routes.forEach(route -> {
//                    List<RoutePoint> routePoints = generateRoutePoints(count, route, addresses);
//                    route.setRoutePoints(routePoints);
//                    try {
//                        dataSourceCore.insert(route);
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                });
//                
//            });
//        });
    }
    
}
