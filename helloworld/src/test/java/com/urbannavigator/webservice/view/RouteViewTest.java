/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.urbannavigator.webservice.view;

import com.urbannavigator.webservice.data.datasources.sql.base.DataSourceCore;
import com.urbannavigator.webservice.data.datasources.sql.base.DataSourceException;
import com.urbannavigator.webservice.data.entities.Category;
import com.urbannavigator.webservice.data.entities.Country;
import com.urbannavigator.webservice.data.entities.Route;
import com.urbannavigator.webservice.data.entities.ShowPlace;
import com.urbannavigator.webservice.data.hibernate.HibernateUtil;
import com.urbannavigator.webservice.domain.entity.Response;
import com.urbannavigator.webservice.util.EntityGenerator;
import com.urbannavigator.webservice.view.entity.RequestCategory;
import com.urbannavigator.webservice.view.entity.RequestRoute;
import com.urbannavigator.webservice.view.entity.RequestRoutePoint;
import com.urbannavigator.webservice.view.entity.RequestShowPlace;
import com.urbannavigator.webservice.view.entity.RequestType;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author valera
 */
public class RouteViewTest {
    
    private RouteView routeView;
    private ShowPlaceView showPlaceView;
    private DataSourceCore dataSourceCore;
    
    public RouteViewTest() {
    }
    
    @BeforeClass
    public static void setUpClass() throws Throwable{
        EntityGenerator.createAndSaveCategories();
        EntityGenerator.createAndSaveTypes();
        EntityGenerator.createAndSaveCountry();
        EntityGenerator.createAndSaveAddress();
        EntityGenerator.createAndSaveRoute();
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        routeView = new RouteView();
        showPlaceView = new ShowPlaceView();
        dataSourceCore = new DataSourceCore(HibernateUtil.getSessionFactory());
       
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetShowPlacesByCountry () {
        System.out.println("Start testGetShowPlacesByCountry");
        Response<List<RequestShowPlace>> responseShowPlacesByCountry = showPlaceView.getShowPlacesByCountry(5);
        assertEquals(responseShowPlacesByCountry.getT().get(0).getId(), 9);
        assertEquals(responseShowPlacesByCountry.getT().get(1).getId(), 10);
        assertEquals(responseShowPlacesByCountry.getT().get(2).getId(), 12);
        assertEquals(responseShowPlacesByCountry.getT().get(3).getId(), 13);
        assertEquals(responseShowPlacesByCountry.getT().get(4).getId(), 16);
        assertEquals(responseShowPlacesByCountry.getT().get(5).getId(), 17);
        assertEquals(responseShowPlacesByCountry.getT().get(6).getId(), 19);
        assertEquals(responseShowPlacesByCountry.getT().get(7).getId(), 20);
        assertEquals(responseShowPlacesByCountry.getT().get(8).getId(), 24);
        assertEquals(responseShowPlacesByCountry.getT().get(9).getId(), 25);
        assertEquals(responseShowPlacesByCountry.getT().get(10).getId(), 27);
        assertEquals(responseShowPlacesByCountry.getT().get(11).getId(), 28);
        assertEquals(responseShowPlacesByCountry.getT().get(12).getId(), 31);
        assertEquals(responseShowPlacesByCountry.getT().get(13).getId(), 32);
        assertEquals(responseShowPlacesByCountry.getT().get(14).getId(), 34);
        assertEquals(responseShowPlacesByCountry.getT().get(15).getId(), 35);
        System.out.println("End testGetShowPlacesByCountry");
    }
    
    @Test
    public void testGetShowPlacesByTown () {
        System.out.println("Start testGetShowPlacesByTown");
        Response<List<RequestShowPlace>> responseShowPlacesByTown = showPlaceView.getShowPlacesByTown(6);
        assertEquals(responseShowPlacesByTown.getT().get(0).getId(), 9);
        assertEquals(responseShowPlacesByTown.getT().get(1).getId(), 10);
        assertEquals(responseShowPlacesByTown.getT().get(2).getId(), 12);
        assertEquals(responseShowPlacesByTown.getT().get(3).getId(), 13);
        assertEquals(responseShowPlacesByTown.getT().get(4).getId(), 16);
        assertEquals(responseShowPlacesByTown.getT().get(5).getId(), 17);
        assertEquals(responseShowPlacesByTown.getT().get(6).getId(), 19);
        assertEquals(responseShowPlacesByTown.getT().get(7).getId(), 20);
        System.out.println("End testGetShowPlacesByTown");
    }
    
    @Test
    public void testGetShowPlacesByDistrict () {
        System.out.println("Start testGetShowPlacesByDistrict");
        Response<List<RequestShowPlace>> responseShowPlacesByDistrict = showPlaceView.getShowPlacesByDistrict(7);
        assertEquals(responseShowPlacesByDistrict.getT().get(0).getId(), 9);
        assertEquals(responseShowPlacesByDistrict.getT().get(1).getId(), 10);
        assertEquals(responseShowPlacesByDistrict.getT().get(2).getId(), 12);
        assertEquals(responseShowPlacesByDistrict.getT().get(3).getId(), 13);
        System.out.println("End testGetShowPlacesByDistrict");
    }
    
    @Test
    public void testGetShowPlacesByStreet () {
        System.out.println("Start testGetShowPlacesByStreet");
        Response<List<RequestShowPlace>> responseShowPlacesByStreet = showPlaceView.getShowPlacesByStreet(8);
        assertEquals(responseShowPlacesByStreet.getT().get(0).getId(), 9);
        assertEquals(responseShowPlacesByStreet.getT().get(1).getId(), 10);
        System.out.println("End testGetShowPlacesByStreet");
    }
    
    @Test 
    public void testGetShowPlacesByLatLon () {
        System.out.println("Start testGetShowPlacesByLatLon");
        Response<List<RequestShowPlace>> responseShowPlacesByCoordinate = 
                showPlaceView.getShowPlacesByCurrentPlace(33.192, 47.076);
        assertEquals(responseShowPlacesByCoordinate.getT().get(0).getId(), 9);
        assertEquals(responseShowPlacesByCoordinate.getT().get(1).getId(), 10);
        System.out.println("End testGetShowPlacesByLatLon");
    }
    
    @Test 
    public void testGetRoutesByLatLon () {
        System.out.println("Start testGetRoutesByLatLon");
        Response<List<RequestRoute>> responseRoutesByCoordinate = 
                routeView.getRoutesByCurrentPlace(33.192, 47.076); //36,37 address
        assertEquals(responseRoutesByCoordinate.getT().get(0).getId(), 60);
        assertEquals(responseRoutesByCoordinate.getT().get(1).getId(), 52);
        assertEquals(responseRoutesByCoordinate.getT().get(2).getId(), 56);
        System.out.println("End testGetRoutesByLatLon");
    }
    
    @Test
    public void testGetRouteByCountry () {
        System.out.println("Start testGetRouteByCountry");
        Response<List<RequestRoute>> responseRouteByCountry = routeView.getRoutesByCountry(5);
        assertEquals(responseRouteByCountry.getT().get(0).getId(), 60);
        assertEquals(responseRouteByCountry.getT().get(1).getId(), 52);
        assertEquals(responseRouteByCountry.getT().get(2).getId(), 56);
        System.out.println("End testGetRouteByCountry");
    }
    
    @Test
    public void testGetRouteByTown () {
        System.out.println("Start testGetRouteByTown");
        Response<List<RequestRoute>> responseRouteByTown = routeView.getRoutesByTown(6);
        assertEquals(responseRouteByTown.getT().get(0).getId(), 60);
        assertEquals(responseRouteByTown.getT().get(1).getId(), 52);
        assertEquals(responseRouteByTown.getT().get(2).getId(), 56);
        System.out.println("End testGetRouteByTown");
    }
    
    @Test
    public void testGetRouteByDistrict () {
        System.out.println("Start testGetRouteByDistrict");
        Response<List<RequestRoute>> responseRouteByDistrict = routeView.getRoutesByDistrict(7);
        assertEquals(responseRouteByDistrict.getT().get(0).getId(), 60);
        assertEquals(responseRouteByDistrict.getT().get(1).getId(), 52);
        assertEquals(responseRouteByDistrict.getT().get(2).getId(), 56);
        System.out.println("End testGetRouteByDistrict");
    }
    
    @Test
    public void testGetRouteByStreet () {
        System.out.println("Start testGetRouteByStreet");
        Response<List<RequestRoute>> responseRouteByStreet = routeView.getRoutesByStreet(8);
        assertEquals(responseRouteByStreet.getT().get(0).getId(), 60);
        assertEquals(responseRouteByStreet.getT().get(1).getId(), 52);
        assertEquals(responseRouteByStreet.getT().get(2).getId(), 56);
        System.out.println("End testGetRouteByStreet");
    }
    
    @Test
    public void testSaveRoute () throws DataSourceException{
        RequestRoute requestRoute = new RequestRoute();//31 35 shoplaces
        requestRoute.setName("New Route 1");
        RequestRoutePoint requestRoutePoint1 = new RequestRoutePoint();
        requestRoutePoint1.setName("New Route Point 1");
        requestRoutePoint1.setLat(12.65765);
        requestRoutePoint1.setLon(20.456364654);
        RequestRoutePoint requestRoutePoint2 = new RequestRoutePoint();
        requestRoutePoint2.setLat(28.65657765);
        requestRoutePoint2.setLon(31.4563646543);
        requestRoutePoint2.setName("New Route Point 2");      
        List<RequestRoutePoint> rrps = Arrays.asList(requestRoutePoint1, requestRoutePoint2);
        requestRoute.setPoints(rrps);
        Response response = routeView.postRoute(requestRoute);
        assertEquals(response.getMessage(), "OK");
        List<Route> rs = 
                dataSourceCore.selectAllItems(Route.class);
        Route r = new Route();
        r.setId(65);
        assertEquals(rs.contains(r), true);
        System.out.println("End testGetRouteByStreet");
    }
    
    @Test
    public void testSaveShowPlace () throws DataSourceException{
        RequestShowPlace requestShowPlace = new RequestShowPlace();
        requestShowPlace.setName("Random name");
        requestShowPlace.setLat(323.123123);
        requestShowPlace.setLon(9.12321);
        requestShowPlace.setStreetId(8);
        requestShowPlace.setDistrictId(7);
        requestShowPlace.setTownId(6);
        requestShowPlace.setCountryId(5);
        Response response = showPlaceView.postRoutesByStreet(requestShowPlace);
        assertEquals(response.getMessage(), "OK");
        List<ShowPlace> showPlaces = dataSourceCore.selectAllItems(ShowPlace.class);
        ShowPlace s = new ShowPlace();
        s.setId(64);
        assertEquals(showPlaces.contains(s), true);
    }
}
